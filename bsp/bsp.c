/*
 * Copyright (c) 2013, 2015-2016 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "bsp.h"

extern  void  cyanSystemTick(void);                         /* Interrupt routine for Core timer (IVG6)     */ 

void bspPllInit(int pmsel, int pssel)
{
	int new_PLL_CTL;	
	*pPLL_DIV = pssel;
	asm("ssync;");
	new_PLL_CTL = (pmsel & 0x3f) << 9;		
	*pSIC_IWR |= 0xffffffff;
	if (new_PLL_CTL != *pPLL_CTL) 
	{
		*pPLL_CTL = new_PLL_CTL;

		/* idle the core to allow the PLL to lock to the new frequency */
		asm("ssync;");
		asm("idle;");
	}
} 

void bspUartInit(unsigned int rate,unsigned int system_clk)
{
	unsigned int div;
	div = system_clk/rate/16;

	*pUART_GCTL=0x0001;
	*pUART_LCR=0x0080;			// DLAB=1 ????DLL ?DLH
	*pUART_DLL=div;
	*pUART_DLH=div>>8; 			//DLL DLH????
	*pUART_LCR=0x0003;			//???? RBR THR ?IER
	*pUART_IER=0x0001;			//??????


	/* UART RX interrupt - IVG10 */
	*pSIC_IAR0 = 0xffffffff;
	*pSIC_IAR1 = 0xf3ffffff;
	*pSIC_IAR2 = 0xffff5fff;

	//*pSIC_IAR2 = 0xffffffff;

	/* enable UART RX interrupt */
	*pSIC_IMASK |= 0x00004000;

	asm("ssync;");
}


void bspCoreTimerInit (int pmsel)
{
    uint32_t scale = 0, count = 0;

    /* enable timer: TMPER */
    *pTCNTL = 1;

    scale = ((BOARD_CLKIN_MHZ * pmsel) >> 2) - 1;
    
    count = (1000000u << 2) / (CYAN_RTOS_TICKS_PER_SEC); /* And COUNT is set per OS_TICKS_PER_SEC and SCALE   */

    *pTSCALE   = scale;
    *pTCOUNT   = count;
    *pTPERIOD  = count;
    
    *pTCNTL    = 0x7;                                   /* Start Timer and set Auto-reload             */

    asm("ssync;");
}

void BSP_InitEBIU (void)
{

    *pEBIU_AMBCTL0 = 0x7bb07bb0;
    *pEBIU_AMBCTL1 = 0x7bb07bb0;
    *pEBIU_AMGCTL  = 0x00ff;

}

void  BSP_InitTimers (void)
{

}

void BSP_IVG7_ISR (void)
{

}

