/*
 * Copyright (c) 2013, 2015-2016 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */
    
#ifndef BSP_H__
#define BSP_H__

#include <sysreg.h>
#include <sys/platform.h>
#include <ccblkfn.h>

#include <kernel/sys/rtos.h>

/* hardware related constants */
/* input frequency in MHz to CLKIN pin */
#define BOARD_CLKIN_MHZ               25

/* PLL core frequency multiplier */
#define BOARD_PLL_MUL                 26

/* system bus frequency divider */
#define BOARD_SYSTEM_CLK_DIV          5

/* calculate the system bus frequency in Hz */
#define BOARD_SYSTEM_CLK_HZ   ((BOARD_CLKIN_MHZ * BOARD_PLL_MUL / BOARD_SYSTEM_CLK_DIV) * 1e6)

/* calculate the system core frequency in Hz */
#define BOARD_CORE_CLK_HZ     ((BOARD_CLKIN_MHZ * BOARD_PLL_MUL) * 1e6)


void  BSP_InitEBIU (void);

void bspPllInit(int pmsel, int pssel);

void  bspCoreTimerInit (int pmsel);

void  BSP_InitTimers();

void bspUartInit(unsigned int rate,unsigned int system_clk);

#endif  /* BSP_H__ */
