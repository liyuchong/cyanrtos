/*
 * Copyright (c) 2013, 2015-2016 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdio.h>
#include <ctype.h>

#include  "../kernel/sys/rtos.h"
#include  "../bsp/bsp.h"
#include <cdefBF533.h>
#include <sys/exception.h>

#include <string.h>

#include "../kernel/sched/scheduler.h"
#include <kernel/sys/kern_lib.h>

#include "../kernel/arch/blackfin/asm/system.h"

#include <kernel/sys/vector.h>
#include "apps.h"
#include <kernel/mt/memory.h>

#include <setjmp.h>
#include <kernel/net/if.h>
#include <kernel/net/in.h>
#include <lib/lisq/lisq.h>

//CircularBuffer inBuf;
//CircularBuffer outBuf;

CircularBuffer ins;

Semaphore sem;

static atomic_t ct1 = ATOMIC_INIT(0);

void sendStr(char *str)
{
    char *p = str;
    
    while (*p)
    {
        *pUART_THR = *p++;
         while(!(*pUART_LSR&0x0020));
    }
}

L1_CODE void sendChar(char c)
{
    *pUART_THR = c;
     while(!(*pUART_LSR&0x0020));
}

L1_CODE static void uartHandler(void)
{
    if(*pUART_LSR & DR)
    {
        /* read UART_RBR register */
        uint8_t c = *pUART_RBR;

        if(c == 3)
        {
        	qprintf("^C\r\n");

        	CyanTCB *tcb = ttyRoot->loginTask;

        	while(tcb->waitingFor)
        	{
        		tcb = tcb->waitingFor;
        	}

        	cyanKillTask(tcb);

        	//cyanSendSignal(sched.run, 0x93);
        }
        else
        {
        	semaphorePost(&sem);

            if(!circularBufferFull(ttyRoot->in))
            {
            	circularBufferWrite(ttyRoot->in, &c, 1);
            }
        }
    }
}

int lsc = 0;

CYAN_TASK void ls(int argc, const char *argv[])
{
	for(;;)
	{
		semaphoreWait(&sem);

		qprintf("wow\r\n");
	}

	for(;;)
	{
		char buf[256] = { 0 };
		circularBufferRead(&inetIn, buf, 256, IPC_BLOCKED);

		uprintf("recv: [%s]\r\n", buf);
	}

	while(1)
	{
		//semaphoreWait(&sem);
		uint8_t ch[1024];
		memset(ch, 0, 1024);
		lsc += circularBufferRead(&ins, ch, 1024, IPC_BLOCKED);


		//printk("%d\r\n", ct1.v);
	}

//	while(1)
//	{
//		qprintf("%d\r\n", ct1.v);
//
//		ct1.v = 0;
//
//		sleep(1);
//	}
}

int last = 0;

extern int w5500d(EtherInterface *eif);

CYAN_TASK void netd(int argc, const char *argv[])
{
	if(argc != 2)
	{
		qprintf("usage: netd interface\r\n");
		return;
	}

	qprintf("netd(%d): interface %s\r\n", getpid(), argv[1]);

	int index = findInterface(argv[1]);

	if(index == -1)
	{
		qprintf("netd(%d): no such interface '%s'\r\n", getpid(), argv[1]);
	}
	else
	{
		w5500d(&interfaces.data[index]);
	}

	// ---------------

    uint32_t pid = 0;
    CyanTCB *tcb = NULL;

    /* verify arguments */
    if(argc < 2)
    {
        goto __print_usage;
    }

    if(!isnum(argv[1]))
    {
        goto __print_usage;
    }

    /* get pid */
    for(int i = 1; i < argc; i++)
    {
        pid = atoi(argv[i]);
        tcb = cyanFindTask(pid);

        if(tcb)
        {

        	KernelMessage *msg = malloc_mt(sizeof(KernelMessage));

        	msg->msgid = KM_KILL;
        	msg->data = tcb;

        	MsgQueue *queue = message(20, MSG_QUEUE_CREATE);

        	msgQueueSend(queue, msg, MSG_QUEUE_INSERT_NORMAL);

            //cyanKillTask(tcb);
        }
        else
        {
            qprintf("kill: kill %d failed: no such task\r\n", pid);
        }
    }

    return;

__print_usage:
    qprintf("usage: kill pid ...\r\n");
}

void Init_EBIU(void)
{
    *pEBIU_AMBCTL0  = 0x7bb07bb0;
    *pEBIU_AMBCTL1  = 0xffc07bb0;
    *pEBIU_AMGCTL   = 0x000f;
}

void Init_SDRAM(void)
{
    *pEBIU_SDRRC = 0x00000817;
    *pEBIU_SDBCTL = 0x00000013;
    *pEBIU_SDGCTL = 0x0091998d;

    ssync();
}

#define SHELL_PROMPT_GREEN "\033[32m"
#define SHELL_PROMPT_CYAN "\033[36m"
#define SHELL_PROMPT_NORMAL "\033[0m"

CYAN_TASK void watch(int argc, const char *argv[])
{
	float interval = 1.0;

	int start = 1;

	if(argc < 2)
	{
		goto __print_usage;
	}

	for(int i = 0; i < argc; i++)
	{
		if(strcmp("-n", argv[i]) == 0 && i < argc)
		{
			interval = atof(argv[i + 1]);
			start = 3;

			if(interval < 0.1 || interval > 60.0)
			{
				goto __print_usage;
			}
		}
	}

	int sleepInt = CYAN_RTOS_TICKS_PER_SEC * interval;

    while(1)
    {
        qprintf("\033[2J");
        qprintf("\033[0;0H");
        qprintf(SHELL_PROMPT_GREEN "Every %.1fs: ", interval);

        for(int i = start; i < argc; i++)
        {
            qprintf("%s ", argv[i]);
        }

        qprintf(SHELL_PROMPT_NORMAL"\r\n\r\n");

        Arguments *arguments = malloc_mt(sizeof(VECTOR(char *)));

        if(!arguments)
        {
            qprintf("shell: command failed because of out of memory\r\n");
            return;
        }

        SV_INIT(arguments, 2, char *);

        for(int i = start; i < argc; i++)
        {
            char *arg = malloc_mt(strlen(argv[i]) + 1);
            strcpy(arg, argv[i]);

            SV_PUSH_BACK(arguments, arg);
        }


        /* the first element of the argument list shall be command name,
         * find the corresponding command according to command name */
        int index = findCommand(SV_AT(arguments, 0));

        if(index == -1)
        {
            /* command not found */
            qprintf("error: command not found: %s\r\n", SV_AT(arguments, 0));

            /* release argument list */
            cyanReleaseArgumentList(arguments);
        }
        else
        {
            CommandItem item = commands[index];

            CyanTCB *tcb = cyanTaskCreate(item.task, arguments, item.stackSize,
                    item.name, item.priority, item.round, false);

            wait(tcb);
        }

        //sleep(1);
        tsleep(sleepInt);
    }

__print_usage:

	qprintf("Usage: \r\n"
			"  watch [options] command \r\n"
			"\r\n"
			"Options: \r\n"
			"  -n <secs>  seconds to wait between updates(0.1 - 60)\r\n");

	return;
}

int countUpdate = 0;

section("L1_code")
void imuHandler(void)
{
    if((*pFIO_FLAG_D & PF0) == PF0)
    {
    	atomicInc(&ct1);

    	countUpdate++;

    	if(countUpdate == 9)
    	{
    		countUpdate = 0;

        	//semaphorePost(&sem);
        	uint8_t c[] = {93,93,93,93,93,93,93,93,93};

        	if(!circularBufferFull(&ins))
        	{
        		circularBufferWrite(&ins, c, 9);
        	}
        	else
        	{
        		printk("buffer full\r\n");
        	}

    	}


    }

    *pFIO_FLAG_C = PF0;
}

CYAN_TASK void launchd(int argc, const char *argv[])
{
    *pFIO_INEN      |= PF0;
    *pFIO_DIR       &=~(PF0);
    *pFIO_EDGE      |= PF0;
    *pFIO_MASKA_D   |= PF0;

    *pFIO_DIR |= PF1;
    *pFIO_DIR |= PF3;



    // ------------

    circularBufferInit(&ins, 8192);

    cyanTaskCreate(iod, NULL, DEFAULT_STACK_SIZE, "iod", 6, 100, true);
    cyanTaskCreate(statd, NULL, DEFAULT_STACK_SIZE, "statd", 2, 100, true);

    // ---

    // XXX: these should be moved to boot script
    exec_cmd("netd& eth0");
    exec_cmd("login& ttys000");

    // register uart handler
    cyanRegisterInterruptHandler(IVG10, uartHandler, true);
    cyanEnableInterruptEntry(IVG10);

    cyanRegisterInterruptHandler(IVG12, imuHandler, true);
    //cyanEnableInterruptEntry(IVG12);

    *pSIC_IMASK |= 0x00080000;

    kernelMessageLoop();
}

int main(void)
{
    Init_EBIU();
    Init_SDRAM();

    /* board setup */
    bspPllInit(BOARD_PLL_MUL, BOARD_SYSTEM_CLK_DIV);
    bspCoreTimerInit(BOARD_PLL_MUL);
    bspUartInit(230400, BOARD_SYSTEM_CLK_HZ);

    uprintfInit(sendChar);

    /* RTOS setup */
    cyanInitRTOS();

    semaphoreInit(&sem, 0);

    cyanTaskCreate(launchd, NULL, DEFAULT_STACK_SIZE, "launchd", 0, 100, true);

    cyanSchedulerStart();
}
