/*
 * Copyright (c) 2013, 2015-2016 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <kernel/sys/rtos.h>
#include <kernel/sys/kern_lib.h>
#include <kernel/sys/vector.h>
#include <lib/lisq/lisq.h>

void lisqCleanUp(struct LisqInterpreter *intp)
{
    struct LisqCommand *c = intp->commands;

    VECTOR(struct LisqCommand *) cmds;
    SV_INIT(&cmds, 16, struct LisqCommand *);

    while(c)
    {
        SV_PUSH_BACK(&cmds, c);
        c = c->next;
    }

    for(int i = 0; i < SV_SIZE(&cmds); i++)
    {
        struct LisqCommand *c = SV_AT(&cmds, i);

        free_lisq(intp, c->name);

        if(c->privdata)
        {
            free_lisq(intp, c->privdata[0]);
            free_lisq(intp, c->privdata[1]);
            free_lisq(intp, c->privdata);
        }

        free_lisq(intp, c);
    }

    SV_RELEASE(&cmds);

    //


    VECTOR(struct LisqVar *) vars;
    SV_INIT(&vars, 16, struct LisqVar *);

    struct LisqVar *v = intp->callframe->vars;
    while (v)
    {
        SV_PUSH_BACK(&vars, v);
        v = v->next;
    }

    for(int i = 0; i < SV_SIZE(&vars); i++)
    {
        free_lisq(intp, SV_AT(&vars, i)->name);
        free_lisq(intp, SV_AT(&vars, i)->val);
        free_lisq(intp, SV_AT(&vars, i));
    }

    SV_RELEASE(&vars);

    free_lisq(intp, intp->result);
    free_lisq(intp, intp->callframe);

    lisq_mem_destroy(intp);
}

int cmdExit(struct LisqInterpreter *i, int argc, char **argv, void *pd)
{
	lisqCleanUp(i);

    cyanKillTask(sched.run);

    return 0;
}

int cmdGetChar(struct LisqInterpreter *i, int argc, char **argv, void *pd)
{
    if (argc != 1 && argc != 2)
        return lisqArityErr(i, argv[0]);

    if(argc == 2)
    	qprintf("%s\r\n", argv[1]);

    char str[] = { __getchar(),'\0'};
    lisqSetResult(i, str);

    return QAQ_OK;
}

int cmdReadline(struct LisqInterpreter *i, int argc, char **argv, void *pd)
{
	char buffer[256];

    if (argc != 1 && argc != 2)
        return lisqArityErr(i, argv[0]);

    if(argc == 1)
    	lisqSetResult(i, readline("", buffer));
    else
    	lisqSetResult(i, readline(argv[1], buffer));

    return QAQ_OK;
}

int cmdSleep(struct LisqInterpreter *i, int argc, char **argv, void *pd)
{
	if(argc != 2)
	{
		return lisqArityErr(i, argv[0]);
	}

	int sec = atoi(argv[1]);
	sleep(sec);

	return QAQ_OK;
}

int cmdMSleep(struct LisqInterpreter *i, int argc, char **argv, void *pd)
{
	if(argc != 2)
	{
		return lisqArityErr(i, argv[0]);
	}

	int sec = atoi(argv[1]);
	msleep(sec);

	return QAQ_OK;
}

#define SHELL_PROMPT __TERM_FG_GREEN "QAQ> " __TERM_RESET
#define MORE_PROMPT  __TERM_FG_GREEN "   > " __TERM_RESET

int lisqExiting(int argc, const char *argv[])
{
    return 0;
}

void lisq(int argc, const char **argv)
{
	sched.run->exiting = lisqExiting;

	size_t memSize = 64 * 1024;

    struct LisqInterpreter interp;
    lisq_mem_alloc(&interp, memSize);

    lisqIntepreterInit(&interp);

    /* system dependent lisq commands */
    lisqRegisterCommand(&interp, "exit",     cmdExit,     NULL);
    lisqRegisterCommand(&interp, "readline", cmdReadline, NULL);
    lisqRegisterCommand(&interp, "getchar",  cmdGetChar,  NULL);
    lisqRegisterCommand(&interp, "sleep",    cmdSleep,    NULL);
    lisqRegisterCommand(&interp, "msleep",   cmdMSleep,   NULL);

    char buf[2048];

    qprintf("\r\n");
    qprintf("QAQ Lisq (Compiled %s, %s) on\r\n", __DATE__, __TIME__);
    qprintf("%s\r\n", __getenv("UNAME"));
    qprintf("Memory: %dKB, use -mem switch to change. \r\n\r\n", memSize / 1024);

    while(1)
    {
        memset(buf, 0, 2048);
        readmore(SHELL_PROMPT, buf);

        /* execute */
        int retcode = lisqEval(&interp, buf);
        if (interp.result[0] != '\0')
        {
            if(retcode != 0)
            {
                qprintf(__TERM_HIGHLIGHT);
            }
            else
            {
                qprintf(__TERM_FG_BLUE);
            }

            qprintf(":= %s\r\n" __TERM_RESET, interp.result);
        }
    }
}
