/*
 * Copyright (c) 2013, 2015-2016 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef APPS_H_
#define APPS_H_

#include <kernel/sys/rtos.h>

typedef struct CommandItem
{
    void (*task)(int argc, const char *argv[]);
    Arguments *arg;
    uint32_t stackSize;
    char *name;
    uint8_t priority;
    uint16_t round;
} CommandItem;


void shell(int argc, const char *argv[]);


// --------

void netd(int argc, const char *argv[]);

void ls(int argc, const char *argv[]);

void watch(int argc, const char *argv[]);

void lisq(int argc, const char **argv);

void ifconfig(int argc, const char **argv);

void login(int argc, const char *argv[]);

const static CommandItem commands[] =
{
    { ls,    NULL, DEFAULT_STACK_SIZE,  "ls",    10, 100 },
    { ifconfig,    NULL, DEFAULT_STACK_SIZE,  "ifconfig",    10, 100 },
    { netd,  NULL, 20480, "netd", 12, 100 },
    { watch,  NULL, DEFAULT_STACK_SIZE, "watch", 4, 100 },

    { lisq,  NULL, 20480, "lisq", 9, 100 },

    /* OS built-in commands */
    { ps,     NULL,  DEFAULT_STACK_SIZE, "ps",     3, 100 },
    { top,    NULL,  DEFAULT_STACK_SIZE, "top",    3, 100 },
    { uname,  NULL, DEFAULT_STACK_SIZE,  "uname",  1, 100 },
    { kill,   NULL, DEFAULT_STACK_SIZE,  "kill",   1, 100 },
    { mem,    NULL, DEFAULT_STACK_SIZE,  "mem",    1, 100 },
    { queues, NULL, DEFAULT_STACK_SIZE,  "queues", 1, 100 },
    { envctl, NULL, DEFAULT_STACK_SIZE,  "env",    1, 100 },

    { login, NULL, DEFAULT_STACK_SIZE, "login", 4, 100 },
    { shell, NULL, DEFAULT_STACK_SIZE, "shell", 5, 100 },

    /* end of command list */
    { NULL }
};

#endif /* APPS_H_ */
