/*
 * Copyright (c) 2013, 2015-2016 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <kernel/sys/rtos.h>
#include <kernel/sys/kern_lib.h>
#include <app/apps.h>

int findCommand(char *const command)
{
    int i;

    char *cmd = malloc_mt(strlen(command) + 1);
    strcpy(cmd, command);

    if(cmd[strlen(cmd) - 1] == '&')
    {
    	cmd[strlen(cmd) - 1] = '\0';
    }

    for(i = 0; commands[i].task != NULL; i++)
    {
        if(strcmp(commands[i].name, cmd) == 0)
        {
        	free_mt(cmd);

            return i;
        }
    }

    free_mt(cmd);
    return -1;
}

int exec(Arguments *arguments)
{
    bool daemon = false;

    if(SV_SIZE(arguments) > 0)
    {
    	char *cmdStr = SV_FIRST_ELEMENT(arguments);

    	if(strlen(cmdStr) == 0)
    	{
    		return 1;
    	}

    	if(cmdStr[strlen(cmdStr) - 1] == '&')
    	{
    		daemon = true;
    	}
    }

    /* the first element of the argument list shall be command name,
     * find the corresponding command according to command name */
    int index = findCommand(SV_AT(arguments, 0));

    if(index == -1)
    {
        /* command not found */
        qprintf("error: command not found: %s\r\n", SV_AT(arguments, 0));

        /* release argument list */
        cyanReleaseArgumentList(arguments);

        return 1;
    }
    else
    {
        CommandItem item = commands[index];

        CyanTCB *tcb =
        		cyanTaskCreate(item.task, arguments, item.stackSize,
                item.name, item.priority, item.round, daemon);

        if(!daemon)
        {
        	wait(tcb);
        }

        return 0;
    }
}

int exec_args(int argc, const char *argv[])
{
	if(argc == 0)
	{
		return 1;
	}

    Arguments *arguments = malloc_mt(sizeof(VECTOR(char *)));

    if(!arguments)
    {
        qprintf("exec_cmd: command failed because of out of memory\r\n");
        return 1;
    }

    SV_INIT(arguments, 2, char *);

    for(int i = 0; i < argc; i++)
    {
    	char *arg = malloc_mt(strlen(argv[i]) + 1);
    	strcpy(arg, argv[i]);

    	SV_PUSH_BACK(arguments, arg);
    }

    return exec(arguments);
}

int exec_cmd(char *cmd)
{
    if(strlen(cmd) == 0)
    {
        return 1;
    }

    Arguments *arguments = malloc_mt(sizeof(VECTOR(char *)));

    if(!arguments)
    {
        qprintf("exec_cmd: command failed because of out of memory\r\n");
        return 1;
    }

    SV_INIT(arguments, 2, char *);

    int16_t lastIndex = 0;
    int16_t i = 0;
    int16_t len = strlen(cmd);

#define __FETCH_ARG()                               \
    char *para = malloc_mt(i - lastIndex + 1);      \
    memcpy(para, cmd + lastIndex, i - lastIndex);   \
    para[i - lastIndex] = '\0';                     \
    SV_PUSH_BACK(arguments, para);

    /* trim head */
    while(cmd[i++] == ' ') lastIndex++;

    /* trim tail */
    while(cmd[len-- - 1] == ' ');
    len++;

    for(; i < len; i++)
    {
        if(cmd[i] == ' ')
        {
            /* ignore continuous space */
            if(i + 1 < len && cmd[i + 1] == ' ')
                continue;

            __FETCH_ARG();

            lastIndex = i + 1;
        }
    }

    __FETCH_ARG();

#undef __FETCH_ARG

    return exec(arguments);
}
