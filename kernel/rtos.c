/*
 * Copyright (c) 2013, 2015-2016 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <string.h>

#include <kernel/sched/scheduler.h>
#include <kernel/sys/rtos.h>
#include <kernel/sched/queue_ops.h>
#include <kernel/net/if.h>

#include <bsp/bsp.h>

#include <kernel/sys/vector.h>

unsigned long irq_flags = 0x1f;

/* current max PID number */
uint32_t pidMax = 0;

/* operating system scheduler */
CyanScheduler sched;

/* indicates if the kernel is running */
uint32_t running;

/* interrupt nesting level */
uint8_t intNesting;

/* pointer to the TCB which is currently running on the CPU */
CyanTCB *cyanTcbCurrent;

/* pointer to the TCB which is ready to be run */
CyanTCB *cyanTcbReady;

/* message queue manager */
MsgQueueManager mqueueMan;

static const char __logo[] = {
    #include "../logo.inc"
};

static void printBootMessage(void)
{
    printk("\r\n");
    printk("%s\r\n", __logo);

    printk("%s\r\n", CYAN_RTOS_UNAME);

    printk("processor: %s\r\n", CYAN_RTOS_CPU_NAME);
    printk(
        "system core frequency: %uMHz, system bus frequency: %uMHz\r\n",
        (uint32_t)(BOARD_CORE_CLK_HZ   / 1e6),
        (uint32_t)(BOARD_SYSTEM_CLK_HZ / 1e6)
    );
}

void cyanInitRTOS(void)
{
    printBootMessage();

    cyanMemInit();

    /* initialize core event vectors
     *    IVG14: context switch trap -> cyanContextSwitch() at context.asm
     *    IVG6 : core timer timeout  -> cyanSystemTick()    at CyanCore.c
     * Note that IVG6 will not be enabled
     * until cyanSchedulerStart() is called. */
    cyanInitEventVector();

    /* initialize scheduler. */
    cyanSchInit(&sched, CYAN_RTOS_TICKS_PER_SEC);

    printk("scheduler: ticks every %d milliseconds\r\n", 1000 / CYAN_RTOS_TICKS_PER_SEC);

    /* initialize message queue */
    msgQueueManagerInit(&mqueueMan);

    ttysInit();

    /* environment variable initialize */
    __envinit();

    setenv("UNAME",    CYAN_RTOS_UNAME,    0);
    setenv("CPU_NAME", CYAN_RTOS_CPU_NAME, 0);

    interfacesInit();

    /* create an idle task
     * priority: 254(lowest)
     * stack size: 256
     * round: 1 */
    cyanTaskCreate(idled, NULL, 256, "idled", 254, 1, true);
}

void cyanSchedulerStart(void)
{
    printk("scheduler: scheduler starting\r\n");

    /* enable core timer interrupt */
    cyanEnableInterruptEntry(IVG6);

    /* initialize spin locks */
    spinLockInit(&sched.ready.lock);
    spinLockInit(&sched.sleep.lock);
    spinLockInit(&sched.stuck.lock);

    /* start first ready task,
     * now the operating system is in multitasking mode */
    cyanStartReadyTask();
}

void sleep(int seconds)
{
    uint16_t tick = CYAN_RTOS_TICKS_PER_SEC * seconds;

    if(tick > 65535)
    {
        printk("sleep: maximum tick 65535\r\n");
        tick = 65535;
    }

    if(seconds <= 0)
    {
        printk("sleep: minimum tick 1\r\n");
        tick = 1;
    }

    tsleep(tick);
}

void msleep(int milliseconds)
{
    uint16_t tick = CYAN_RTOS_TICKS_PER_SEC * milliseconds / 1000;

    if(tick > 65535)
    {
        printk("sleep: maximum tick 65535\r\n");
        tick = 65535;
    }

    if(milliseconds <= 0)
    {
        printk("sleep: minimum tick 1\r\n");
        tick = 1;
    }

    tsleep(tick);
}

L1_CODE void cyanInterruptEnter(void)
{

}

L1_CODE void cyanInterruptExit(void)
{
    if(running)
    {
        /* enter critical section */
        cyanDisableIRQ();
        {
            /* we're exiting the interrupt, so decrement the interrupt
             * nesting count */
            if(intNesting > 0)
            {
                intNesting--;
            }

            /* context switch shall happen only when there's no nesting */
            if(intNesting == 0)
            {
                /* select a task to run */
                cyanSchSelectTask(&sched);

                /* we have different task to run */
                if(cyanTcbCurrent != sched.run)
                {
                    cyanTcbReady = sched.run;
                    sched.run->stat.contextSwitch++;

                    /* interrupt level context switch */
                    cyanContextSwitchISR();
                }
            }
        }
        cyanEnableIRQ();
    }
}

L1_CODE void cyanSystemTick(void)
{
    cyanSchTick(&sched);
}

void __assertionFailed(const char *msg)
{
    printk(__TERM_HIGHLIGHT);
    printk("assertion failed: %s", msg);
    printk(__TERM_RESET);
    panic("kernel level assertion failed:");
}

void kernelMessageLoop(void)
{
    /* wait for message */
    MsgQueue *queue = message(20, MSG_QUEUE_CREATE);

    while(1)
    {
        kassert(queue);

        //heap_malloc
        KernelMessage *data = msgQueueRecv(queue, IPC_BLOCKED);

        switch(data->msgid)
        {
            case KM_KILL:
            {
                CyanTCB *tcb = data->data;
                //qprintf("kill: %s\r\n", tcb->name);

                cyanKillTask(tcb);

                break;
            }

            default: qprintf("unknown msg id\r\n");
        }

        free_mt(data);
    }
}
