/*
 * Copyright (c) 2013, 2015-2016 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */
#include <kernel/sched/scheduler.h>
#include <kernel/sys/rtos.h>
#include <kernel/sched/queue_ops.h>
#include <kernel/sys/vector.h>

static inline void __updateTcbStat(CyanScheduler *sch, CyanTCB *tcb)
{
    tcb->stat.cpuUsage = (tcb->stat.periodTime * 1000) / (sch->priodTick);
    tcb->stat.periodTime = 0;
}

static int cyanTaskStackFree(CyanTCB *tcb)
{
    int stkFree = 0;

    stack_t *stk = tcb->stackPtr;

    while (*stk++ == (stack_t)0)
    {
        stkFree++;
    }

    return stkFree;
}

static inline int __updateQueue(CyanScheduler *sch, CyanTcbQueue *queue)
{
    CyanTCB *tcb;
    int count = 0;

    spinLock(&queue->lock);
    STAILQ_FOREACH(tcb, &queue->q, next)
    {
        __updateTcbStat(&sched, tcb);
        tcb->stackFree = cyanTaskStackFree(tcb);
        count++;
    }
    spinUnlock(&queue->lock);

    return count;
}

/**
 *  Statistic daemon
 */
CYAN_TASK void statd(int argc, const char *argv[])
{
    while(1)
    {
        int flags;
        local_irq_save(flags);

        int count = 0;
        count += __updateQueue(&sched, &sched.ready);
        count += __updateQueue(&sched, &sched.sleep);
        count += __updateQueue(&sched, &sched.stuck);

        sched.priodTick = 0;

        local_irq_restore(flags);

        sleep(1);
    }
}

/**
 *  IO daemon
 */
CYAN_TASK void iod(int argc, const char *argv[])
{
    while(1)
    {
        char buf[1024] = {0};

        int n = circularBufferRead(ttyRoot->out, (uint8_t *)buf, 1024, IPC_BLOCKED);

        uputs(buf, n);
    }
}

/**
 *  Idle daemon
 */
CYAN_TASK void idled(int argc, const char *argv[])
{
    while(1)
    {
        /* yadi yadi ya */
    }
}
