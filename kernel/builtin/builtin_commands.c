/*
 * Copyright (c) 2013, 2015-2016 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */
#include <kernel/sys/rtos.h>

#include <kernel/sched/scheduler.h>
#include <kernel/sched/queue_ops.h>

#include <kernel/sys/kern_lib.h>
#include <kernel/sys/vector.h>
#include <kernel/net/if.h>
#include <kernel/net/in.h>

/**
 * ENV(1) CyanRTOS General Commands Manual
 *
 * NAME
 *     env - environment variables control
 *
 * SYNOPSIS
 *     env get|set [name] [value]
 *
 * DESCRIPTION
 *     The env utility controls/reads the environment variables of the
 *     operating system. It can either get or set an environment variable.
 *
 *     get     Get the value of an environment variable. Should followed by the
 *             name of the variable. If the variable does not exist,
 *             then nothing will be printed.
 *
 *     set     Set the value of an environment variable. Should followed by the
 *             name of the variable. If the variable is already set, this
 *             operation overwrites the original value.
 */
CYAN_TASK void envctl(int argc, const char *argv[])
{
    if(argc != 4 && argc != 3)
    {
        goto __arg_error;
    }

    /* Environment variable set */
    if(strcmp(argv[1], "set") == 0)
    {
        if(argc != 4)
        {
            goto __arg_error;
        }

        const char *key = argv[2];
        const char *value = argv[3];

        setenv(key, value, 1);
    }

    /* Environment variable get */
    else if(strcmp(argv[1], "get") == 0)
    {
        if(argc != 3)
        {
            goto __arg_error;
        }

        char *e = getenv(argv[2]);

        if(!e)
        {
        }
        else
        {
            qprintf("%s\r\n", e);
        }
    }

    /* operation not recognized */
    else
    {
        goto __arg_error;
    }

    return;

__arg_error:
    qprintf("usage: env get|set [name] [value]\r\n");

    return;
}

/* -------------------------------------------------------------------------- */

static void memPrint(void)
{
    qprintf("Memory: system heap: free: %dK, total: %dK. OPC: %d\r\n",
            atomicRead(&memFreeCounter) / 1000,
            TLSF_POOL_SIZE / 1000,
            atomicRead(&memOperationCounter));
}

/**
 * MEM(1) CyanRTOS General Commands Manual
 *
 * NAME
 *     mem - memory status & allocator check
 *
 * SYNOPSIS
 *     mem
 *
 * DESCRIPTION
 *     The mem utility prints the current memory allocator status and perform
 *     a memory allocator status check.
 */
CYAN_TASK void mem(int argc, const char *argv[])
{
    qprintf("%s\r\n", MEM_MAN_INFO);

    memPrint();

    int ret = memHeapCheck();

    if(ret != 0)
    {
        qprintf("Memory heap check: Failed: %d\r\n", ret);
    }
    else
    {
        qprintf("Memory heap check: OK\r\n");
    }
}

/* -------------------------------------------------------------------------- */

/**
 * PS(1) CyanRTOS General Commands Manual
 *
 * NAME
 *     ps - process status
 *
 * SYNOPSIS
 *     ps
 *
 * DESCRIPTION
 * The ps utility displays a header line, followed by lines containing
 * information about all of your tasks.
 */
CYAN_TASK void ps(int argc, const char *argv[])
{
    uint32_t flag = 0;

    for(int i = 1; i < argc; i++)
    {
        if(strcmp("-r", argv[i]) == 0)
            flag |= P_RR;

        if(strcmp("-s", argv[i]) == 0)
            flag |= P_STK;

        if(strcmp("-w", argv[i]) == 0)
            flag |= P_WAIT;
    }

    memPrint();
    cyanSchPrintStatus(&sched, flag);

    return;
}

CYAN_TASK void top(int argc, const char *argv[])
{
    uint32_t flag = 0;

    for(int i = 1; i < argc; i++)
    {
        if(strcmp("-r", argv[i]) == 0)
            flag |= P_RR;

        if(strcmp("-s", argv[i]) == 0)
            flag |= P_STK;
    }

    while(1)
    {
        qprintf("\033[2J");
        qprintf("\033[0;0H");

        memPrint();
        cyanSchPrintStatus(&sched, flag);

        sleep(1);
    }
}

/* -------------------------------------------------------------------------- */

/**
 * KILL(1) CyanRTOS General Commands Manual
 *
 * NAME
 *     kill - kill tasks
 *
 * SYNOPSIS
 *     kill pid, ...
 *
 * DESCRIPTION
 */
CYAN_TASK void kill(int argc, const char *argv[])
{
    uint32_t pid = 0;
    CyanTCB *tcb = NULL;

    /* verify arguments */
    if(argc < 2)
    {
        goto __print_usage;
    }

    if(!isnum(argv[1]))
    {
        goto __print_usage;
    }

    /* get pid */
    for(int i = 1; i < argc; i++)
    {
        pid = atoi(argv[i]);
        tcb = cyanFindTask(pid);

        if(tcb)
        {
            cyanKillTask(tcb);
        }
        else
        {
            qprintf("kill: kill %d failed: no such task\r\n", pid);
        }
    }

    return;

__print_usage:
    qprintf("usage: kill pid ...\r\n");

    return;
}

/* -------------------------------------------------------------------------- */

/**
 * QUEUES(1) CyanRTOS General Commands Manual
 *
 * NAME
 *     queues - queues utility
 *
 * SYNOPSIS
 *     queues
 *
 * DESCRIPTION
 */
CYAN_TASK void queues(int argc, const char *argv[])
{
    qprintf("Max number of queues: %d\r\n", MSG_QUEUE_MAX_QUEUE);
    qprintf("KEY  LEN   PID  TASK\r\n");

    for(int i = 0; i < MSG_QUEUE_MAX_QUEUE; i++)
    {
        MsgQueue *queue = mqueueMan.queues[i];

        if(queue)
        {
            CyanTCB *tcb = (CyanTCB *)STACK_TOP(&queue->tcbs);
            qprintf("%3d %4d", i, queue->len);

            if(STACK_COUNT(&queue->tcbs) > 0)
            {
                for(int i = 0; i < queue->tcbs.top; i++)
                {
                    CyanTCB *m = queue->tcbs.s[i];
                    qprintf(" %5d  %s", m->pid, m->name);
                    qprintf("\r\n        ");
                }

            }
            else
            {
                qprintf(" -");
            }

            qprintf("%d tasks", STACK_COUNT(&queue->tcbs));

            qprintf("\r\n");
        }
    }
}

/* -------------------------------------------------------------------------- */

/**
 * UNAME(1) CyanRTOS General Commands Manual
 *
 * NAME
 *     uname - Print operating system name
 *
 * SYNOPSIS
 *     uname
 *
 * DESCRIPTION
 */
CYAN_TASK void uname(int argc, const char *argv[])
{
    qprintf("%s\r\n", getenv("UNAME"));

    return;
}

/* -------------------------------------------------------------------------- */

#define PRINT_IP4(_ip)                          \
        qprintf("%d.%d.%d.%d",                  \
                   (_ip) & 0xff,                \
                   ((_ip) & 0x0000ff00) >> 8,   \
                   ((_ip) & 0x00ff0000) >> 16,  \
                   ((_ip) & 0xff000000) >> 24);

int findInterface(const char *ifname)
{
    int index = -1;
    for(int i = 0; i < SV_SIZE(&interfaces); i++)
    {
        if(strcmp(SV_AT(&interfaces, i).ifname, ifname) == 0)
        {
            index = i;
            break;
        }
    }

    return index;
}

void interfacePrintConfig(EtherInterface *eif)
{
    qprintf("(");
    switch(eif->speed)
    {
        case INET_SPD_10MTX:  qprintf("10baseT ");  break;
        case INET_SPD_100MTX: qprintf("100baseT "); break;
        default: break;
    }

    qprintf("<");
    switch(eif->duplex)
    {
        case INET_DUPLEX_HALF: qprintf("half-duplex"); break;
        case INET_DUPLEX_FULL: qprintf("full-duplex"); break;
        default: break;
    }
    qprintf(">");
    qprintf(")");
}

/**
 * IFCONFIG(1) CyanRTOS General Commands Manual
 *
 * NAME
 *     ifconfig - Configure network interface parameters
 *
 * SYNOPSIS
 *     ifconfig interface [address] [ether|hw|gw] [address]
 *
 * DESCRIPTION
 */
CYAN_TASK void ifconfig(int argc, const char *argv[])
{
    if(argc == 1)
    {
        for(int i = 0; i < SV_SIZE(&interfaces); i++)
        {
            EtherInterface eif = SV_AT(&interfaces, i);

            qprintf("%s: ", eif.ifname);
            qprintf("\tether %02x:%02x:%02x:%02x:%02x:%02x\r\n",
                    eif.hwaddr[0], eif.hwaddr[1], eif.hwaddr[2],
                    eif.hwaddr[3], eif.hwaddr[4], eif.hwaddr[5]);
            qprintf("\tinet ");

            PRINT_IP4(eif.ipaddr);
            qprintf(" netmask ");
            qprintf("0x%08x", htonl(eif.netmask));
            qprintf(" broadcast ");
            PRINT_IP4(eif.ipaddr | (~eif.netmask));
            qprintf("\r\n\tgateway ");
            PRINT_IP4(eif.gateway);

            qprintf("\r\n\tmedia: autoselect");

            /* print speed and duplex status */
            if(eif.speed && eif.duplex)
            {
                interfacePrintConfig(&eif);
            }

            qprintf("\r\n\trx %dKiB(%d packets) tx %dKiB(%d packets)",
                    eif.rx / 1000, eif.rx_packets, eif.tx / 1000, eif.tx_packets);
        }

        qprintf("\r\n");
    }
    else if(argc == 3)
    {
        /* configure IP address */
        int index = findInterface(argv[1]);

        if(index == -1)
        {
            goto __err_if_not_exist;
        }

        SV_AT(&interfaces, index).ipaddr = inet_addr(argv[2]);
    }
    else if(argc == 4)
    {
        /* configure hardware address */
        int index = findInterface(argv[1]);

        if(index == -1)
        {
            goto __err_if_not_exist;
        }

        uint8_t hwaddr[6];
        if(hw_addr(argv[3], hwaddr) != 0)
        {
            qprintf("ifconfig: invalid hardware address\r\n");
            return;
        }

        memcpy(SV_AT(&interfaces, index).hwaddr, hwaddr, 6);
    }
    else if(argc == 5)
    {
        /* configure IP address and subnet mask / gateway address */
        int index = findInterface(argv[1]);

        if(index == -1)
        {
            goto __err_if_not_exist;
        }

        /* modify ip address */
        SV_AT(&interfaces, index).ipaddr = inet_addr(argv[2]);

        if(strcmp(argv[3], "netmask") == 0)
        {
            SV_AT(&interfaces, index).netmask = inet_addr(argv[4]);
        }
        else if(strcmp(argv[3], "gw") == 0)
        {
            SV_AT(&interfaces, index).gateway = inet_addr(argv[4]);
        }
        else
        {
            goto __print_usage;
        }
    }
    else
    {
        goto __print_usage;
    }

    return;

__print_usage:
    qprintf("usage: ifconfig [interface [address_family] [address]]\r\n");
    return;

__err_if_not_exist:
    qprintf("ifconfig: interface %s does not exist\r\n", argv[1]);
    return;
}

/* -------------------------------------------------------------------------- */

/**
 * LOGIN(1) CyanRTOS General Commands Manual
 *
 * NAME
 *     login - Log into the operating system
 *
 * SYNOPSIS
 *     login tty
 *
 * DESCRIPTION
 */
CYAN_TASK void login(int argc, const char *argv[])
{
    if(argc != 2)
    {
        printk("login: target IO stream not specified\r\n");
        return;
    }

    msleep(100);

    CyanTCB *tcb = sched.run;

    /* find the specified TTY */
    TTY *tty = ttysGet(argv[1]);

    /* see if this TTY exists, if not, just return */
    if(!tty)
    {
        printk("login(%d): failed: no such tty: %s\r\n",
                getpid(), argv[1]);
        return;
    }

    /* return if this TTY has been logged in i.e. loginTask is not NULL */
    if(tty->loginTask)
    {
        printk("login(%d): failed: already logged: %s in\r\n",
                getpid(), argv[1]);
        return;
    }

    /* modify my TTY, and assign me to the TTY's login pointer */
    tcb->tty = tty;
    tcb->tty->loginTask = tcb;

    qprintf("login(%d): login on %s\r\n", getpid(), tty->name);

    /* start shell, now this shell will be interfacing
     * with the specified TTY. */
    exec_cmd("shell");
}
