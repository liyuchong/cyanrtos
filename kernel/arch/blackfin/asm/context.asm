/*
 * Copyright (c) 2013, 2015-2016 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "inc.asm"

/* the task which ready to be run on the CPU */
.global  _cyanStartReadyTask;

/* context switch function */
.global  _cyanContextSwitch;

/* context switch function from an ISR */
.global  _cyanContextSwitchISR;

/* check signal routine */
.extern _cyanCheckSignal;

.section program;

/**
 * start the reay task, this is the first multitasking task to be run
 */
_cyanStartReadyTask:    
    LOADA(P1, _cyanTcbReady);
    P2     = [ P1 ];
    SP     = [ P2 ];
    LOADA(P1, _running);
    R1 = 1;
    [ P1 ] = R1;

    P1     = 140;
    SP     = SP + P1;
    RETS   = [ SP ++ ];
    SP    += 12;

    R1     = 0;           
    LC0    = R1; LC1 = R1;
    L0     = R1; L1 = R1;
    L2     = R1; L3 = R1;

    R2     = [ SP ++ ];
    R1     = [ SP ++ ];
    SP    += 8;
    R0     = [ SP ++ ];

_cyanStartReadyTask.end:
    RTS;

/**
 * perform context switch
 */
_cyanContextSwitch:
    [ -- SP ]    = R0;
    [ -- SP ]    = P1;
    [ -- SP ]    = RETS;
    [ -- SP ]    = R1;
    [ -- SP ]    = R2;
    [ -- SP ]    = P0;
    [ -- SP ]    = P2;
    [ -- SP ]    = ASTAT;
    R1           = RETI;
    
    [ -- SP ]    = R1;
    [ -- SP ]    = (R7:3, P5:3);
    [ -- SP ]    = FP;
    [ -- SP ]    = I0;
    [ -- SP ]    = I1;
    [ -- SP ]    = I2;
    [ -- SP ]    = I3;
    [ -- SP ]    = B0;
    [ -- SP ]    = B1;
    [ -- SP ]    = B2;
    [ -- SP ]    = B3;
    [ -- SP ]    = L0;
    [ -- SP ]    = L1;
    [ -- SP ]    = L2;
    [ -- SP ]    = L3;
    [ -- SP ]    = M0;
    [ -- SP ]    = M1;
    [ -- SP ]    = M2;
    [ -- SP ]    = M3;
    R1.L         = A0.x;
    [ -- SP ]    = R1;  
    R1           = A0.w;
    [ -- SP ]    = R1;  
    R1.L         = A1.x;
    [ -- SP ]    = R1;  
    R1           = A1.w;
    [ -- SP ]    = R1;  
    [ -- SP ]    = LC0; 
    R3           = 0;   
    LC0          = R3;  
    [ -- SP ]    = LC1; 
    R3           = 0;   
    LC1          = R3;  
    [ -- SP ]    = LT0; 
    [ -- SP ]    = LT1; 
    [ -- SP ]    = LB0; 
    [ -- SP ]    = LB1; 
    L0           = 0 ( X );
    L1           = 0 ( X );
    L2           = 0 ( X );
    L3           = 0 ( X );

    LOADA(P4, _cyanTcbCurrent);
    P5           = [ P4 ];
    [ P5 ]       = SP;

    /* cyanTcbCurrent = cyanTcbReady */ 
    LOADA(P3, _cyanTcbReady);
    P5           = [ P3 ];
    [ P4 ]       = P5;

_cyanContextSwitch_modify_SP:
    SP           = [ P5 ];

_cyanContextSwitch.end:
_cyanContextSwitch_check_signal:
    /* check task's signal */
    /* since we're in the middle of the context switch, the previous
     * task's stack is saved and the next task's stack will be loaded later,
     * therefore we can call any C routines safely, no worries. */
    C_RUNTIME_STACK_INIT(0x0)
    CALL.X _cyanCheckSignal;
    C_RUNTIME_STACK_DELETE()
    
_cyanContextSwitch_resume_task:
    LB1          = [ SP ++ ];
    LB0          = [ SP ++ ];
    LT1          = [ SP ++ ];
    LT0          = [ SP ++ ];
    LC1          = [ SP ++ ];
    LC0          = [ SP ++ ];
    R0           = [ SP ++ ];
    A1           = R0;       
    R0           = [ SP ++ ];
    A1.x         = R0.L;     
    R0           = [ SP ++ ];
    A0           = R0;       
    R0           = [ SP ++ ];
    A0.x         = R0.L;     
    M3           = [ SP ++ ];
    M2           = [ SP ++ ];
    M1           = [ SP ++ ];
    M0           = [ SP ++ ];
    L3           = [ SP ++ ];
    L2           = [ SP ++ ];
    L1           = [ SP ++ ];
    L0           = [ SP ++ ];
    B3           = [ SP ++ ];
    B2           = [ SP ++ ];
    B1           = [ SP ++ ];
    B0           = [ SP ++ ];
    I3           = [ SP ++ ];
    I2           = [ SP ++ ];
    I1           = [ SP ++ ];
    I0           = [ SP ++ ];
    FP           = [ SP ++ ];
    (R7:3, P5:3) = [ SP ++ ];
    RETI         = [ SP ++ ];
    ASTAT        = [ SP ++ ];
    P2           = [ SP ++ ];
    P0           = [ SP ++ ];
    R2           = [ SP ++ ];
    R1           = [ SP ++ ];
    RETS         = [ SP ++ ];
    P1           = [ SP ++ ];
    R0           = [ SP ++ ];
    RTI;

_cyanContextSwitchISR:
    /* cyanTcbCurrent = cyanTcbReady */
    LOADA(P0, _cyanTcbCurrent);
    LOADA(P1, _cyanTcbReady);
    P2 = [ P1 ];
    [ P0 ]  = P2;
    
_cyanContextSwitchISR_modify_SP:
    
    R0      = [ P2 ];
    R0     += -8;
    
    [ FP ]  = R0;
    
    SP     += -4;
    RETI    = [ SP++ ];
    
_cyanContextSwitchISR.end:
    RTS;
