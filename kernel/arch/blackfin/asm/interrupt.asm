/*
 * Copyright (c) 2013, 2015-2016 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "inc.asm"

/* global functions */

/* interrupt management functions */
/* enable an interrupt entry */
.global  _cyanEnableInterruptEntry;
/* disable an interrupt entry */
.global  _cyanDisableInterruptEntry;

/* interrupt service response handling functions */
/* nesting ISR */
.global  _cyanNestingISR;
/* non-nesting ISR */
.global  _cyanNonNestingISR;

/* ISR entry function */
.global  _cyanIsrEntry;

/* IST exit function */
.global  _cyanIsrExit;

.section program;

/**
 * enable a given interrupt entry
 */
_cyanEnableInterruptEntry:
    R1   = 1;
    R1 <<= R0;
    CLI    R0;
    R1   = R1 | R0;
    STI    R1;
       
_cyanEnableInterruptEntry.end:
    RTS;

/**
 * disable a given interrupt entry
 */
_cyanDisableInterruptEntry:
    R1   =  1;
    R1 <<=  R0;
    R1   = ~R1;
    CLI     R0;
    R1   =  R1 & R0;
    STI     R1;
   
_cyanDisableInterruptEntry.end:
    RTS;

/**
 *  Prepare for ISRs with interrupt nesting enabled
 *  
 *  This function save the current context then calls cyanInterruptHandler() 
 */
_cyanNestingISR:
    [ -- SP ] = R0;
    [ -- SP ] = P1;
    [ -- SP ] = RETS;
    R0        = NESTED;
    
    CALL.X _cyanIsrEntry;
    
    WORKAROUND_05000283()
    C_RUNTIME_STACK_INIT(0x0)
    
    CALL.X _cyanInterruptHandler;
    SP     += -4;
    RETI    = [ SP++ ];
    CALL.X _cyanInterruptExit;
    
    C_RUNTIME_STACK_DELETE()
    JUMP.X _cyanIsrExit; 

_cyanNestingISR.end:
    NOP;

/**
 *  Prepare for ISRs with interrupt nesting disabled
 *  
 *  This function save the current context then calls cyanInterruptHandler() 
 */
_cyanNonNestingISR:
    [ -- SP ] = R0;
    [ -- SP ] = P1;
    [ -- SP ] = RETS;
    R0        = NOT_NESTED;
    
    CALL.X _cyanIsrEntry;
    
    WORKAROUND_05000283()
    C_RUNTIME_STACK_INIT(0x0)
    
    CALL.X _cyanInterruptHandler;
    CALL.X _cyanInterruptExit;
    
    C_RUNTIME_STACK_DELETE()
    JUMP.X _cyanIsrExit;

_cyanNonNestingISR.end:
    NOP;
    

/**
 *  Interrupt entry function for ISRs
 */
_cyanIsrEntry:
interrupt_entry_management:
    [ -- SP ] = R1;
    [ -- SP ] = R2;
    [ -- SP ] = P0;
    [ -- SP ] = P2;
    [ -- SP ] = ASTAT;
    
    LOADA(P0 , _running);
    R2        = B [ P0 ] ( Z );
    CC        = R2 == 1;
    
    /* skip if rtos is not running */
    IF ! CC JUMP interrupt_entry_management.end;
    LOADA(P0, _intNesting);
    R2        = B [ P0 ] ( Z );
    R1        = 255 ( X );
    CC        = R2 < R1;
    
    /* skip if intNesting > 255 */
    IF ! CC JUMP interrupt_entry_management.end;
    R2        = R2.B (X);
    
    /* intNesting++ */
    R2       += 1;
    B [ P0 ]  = R2;
    CC        = R2 == 1;
    
    /* skip if intNesting != 1 */
    IF ! CC JUMP interrupt_entry_management.end;
    LOADA(P2, _cyanTcbCurrent);
    P0        = [ P2 ];
    R2        = SP;
    R1        = 144;
    R2        = R2 - R1;
    [ P0 ]    = R2;

interrupt_entry_management.end:
    NOP;

interrupt_cpu_save_context:
    CC        = R0 == NESTED;
    IF !CC JUMP non_reetrant_isr;
      
reetrant_isr:
    /* push RETI onto stack if this is an reentrant ISR */
    [ -- SP ] = RETI;
    
    JUMP save_remaining_context;
    
non_reetrant_isr:   
    /* save RETI through R1 if this is an non-reentrant ISR */  
    R1        = RETI;
    [ -- SP ] = R1;
    
save_remaining_context:    
    [ -- SP ] = (R7:3, P5:3);
    [ -- SP ] = FP;
    [ -- SP ] = I0;
    [ -- SP ] = I1;
    [ -- SP ] = I2;
    [ -- SP ] = I3;
    [ -- SP ] = B0;
    [ -- SP ] = B1;
    [ -- SP ] = B2;
    [ -- SP ] = B3;
    [ -- SP ] = L0;
    [ -- SP ] = L1;
    [ -- SP ] = L2;
    [ -- SP ] = L3;
    [ -- SP ] = M0;
    [ -- SP ] = M1;
    [ -- SP ] = M2;
    [ -- SP ] = M3;
    R1.L      = A0.x;
    [ -- SP ] = R1;
    R1        = A0.w;
    [ -- SP ] = R1;
    R1.L      = A1.x;
    [ -- SP ] = R1;
    R1        = A1.w;
    [ -- SP ] = R1;
    [ -- SP ] = LC0;
    R3        = 0;
    LC0       = R3;
    [ -- SP ] = LC1;
    R3        = 0;
    LC1       = R3;
    [ -- SP ] = LT0;
    [ -- SP ] = LT1;
    [ -- SP ] = LB0;
    [ -- SP ] = LB1;
    L0        = 0 ( X );
    L1        = 0 ( X );
    L2        = 0 ( X );
    L3        = 0 ( X );

interrupt_cpu_save_context.end:
_cyanIsrEntry.end:
    RTS;

/**
 *  Interrupt entry function for ISRs
 */
_cyanIsrExit:
interrupt_cpu_restore_context:    
    LB1   = [ SP ++ ];
    LB0   = [ SP ++ ];
    LT1   = [ SP ++ ];
    LT0   = [ SP ++ ];
    LC1   = [ SP ++ ];
    LC0   = [ SP ++ ];
    R0    = [ SP ++ ];
    A1    = R0;       
    R0    = [ SP ++ ];
    A1.x  = R0.L;     
    R0    = [ SP ++ ];
    A0    = R0;       
    R0    = [ SP ++ ];
    A0.x  = R0.L;     
    M3    = [ SP ++ ];
    M2    = [ SP ++ ];
    M1    = [ SP ++ ];
    M0    = [ SP ++ ];
    L3    = [ SP ++ ];
    L2    = [ SP ++ ];
    L1    = [ SP ++ ];
    L0    = [ SP ++ ];
    B3    = [ SP ++ ];
    B2    = [ SP ++ ];
    B1    = [ SP ++ ];
    B0    = [ SP ++ ];
    I3    = [ SP ++ ];
    I2    = [ SP ++ ];
    I1    = [ SP ++ ];
    I0    = [ SP ++ ];
    FP    = [ SP ++ ];
    (R7:3, P5:3) = [ SP ++ ];
    RETI  = [ SP ++ ];
    ASTAT = [ SP ++ ];
    P2    = [ SP ++ ];
    P0    = [ SP ++ ];
    R2    = [ SP ++ ];
    R1    = [ SP ++ ];
    RETS  = [ SP ++ ];
    P1    = [ SP ++ ];

interrupt_cpu_restore_context.end:
    R0    = [ SP ++ ];
    RTI;

    NOP;       
    NOP;

_cyanIsrExit.end:
    NOP;
    