/*
 * Copyright (c) 2013, 2015-2016 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SYSTEM_H__
#define SYSTEM_H__

/* save current IRQ flags and disable the IRQ */
#define cyanDisableIRQ()        save_and_cli(irq_flags)

/* restore the previously saved IRQ flags */
#define cyanEnableIRQ()         local_irq_load(irq_flags)

/* save current IRQ flags to a variable and disable the IRQ */
#define local_irq_save(x)       save_and_cli(x)

/* restore the IRQ flags from a variable */
#define local_irq_restore(x) 	local_irq_load(x)

/* CyanCore.c */
extern unsigned long irq_flags;

#define local_irq_load(__flags)     \
    __asm__ __volatile__(           \
        "sti %0;"                   \
        :                           \
        : "d" (__flags)             \
    )


#define save_and_cli(x)             \
    __asm__ __volatile__(           \
        "cli %0;"                   \
        : "=&d" (x)                 \
    )

static inline int cycles()
{
    int ret;
    
    __asm__ __volatile__(
        "%0 = CYCLES;\n\t"
        : "=&d" (ret)
        :
        : "R1"
   );

   return ret;
}

#endif /* SYSTEM_H__ */
