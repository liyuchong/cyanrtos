/*
 * Copyright (c) 2013, 2015-2016 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "../cpu.h"

/* util marcos */
#define  UPPER_( x )   (((x) >> 16) & 0x0000FFFF)  
#define  LOWER_( x )   ((x) & 0x0000FFFF)  
#define  LOAD(x, y)    x##.h = UPPER_(y); x##.l = LOWER_(y)
#define  LOADA(x, y)   x##.h = y; x##.l = y

/* setup and destroy C runtime stack */
#define C_RUNTIME_STACK_INIT(frame_size)                                \
    LINK frame_size;                                                    \
    SP += -12;        /* make space for outgoing arguments     */
                      /* when calling C-functions              */    
                        
#define C_RUNTIME_STACK_DELETE()                                        \
    UNLINK;

/* 05000283 workaround */
#define WORKAROUND_05000283()                                           \
    CC = R0 == R0;   /* always true                            */       \
    P0.L = 0x14;     /* MMR space - CHIPID                     */       \
    P0.H = 0xffc0;                                                      \
    IF CC JUMP 4;                                                       \
    R0 =  [ P0 ];    /* bogus MMR read that is speculatively   */
                     /* read and killed - never executed       */ 

/* external interrupt handler functions */
.extern _cyanInterruptHandler;
.extern _cyanInterruptEnter;
.extern _cyanInterruptExit;

.extern _cyanTaskExit;

/* external variables */
.extern _intNesting;
.extern _running;
.extern _cyanTcbCurrent;
.extern _cyanTcbReady;
