/*
 * Copyright (c) 2013, 2015-2016 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "cpu.h"

#include <bsp/bsp.h>
#include <kernel/ipc/fifo.h>

extern void cyanTaskExit(void);

void cyanInitEventVector(void)
{
    /* event table base address */
    volatile uint32_t *evt = ((volatile uint32_t*)EVENT_VECTOR_TABLE_ADDR);

    /* IVG14 -> context switch */
    /* register IVG14: context switch interrupt handler */
    evt[IVG14] = (uint32_t)&cyanContextSwitch;

    /* enable IVG14: trap */
    cyanEnableInterruptEntry(IVG14);

    /* IVG6 -> core timer out */
    /* register IVG6: core timer -> scheduler tick */
    cyanRegisterInterruptHandler(IVG6, cyanSystemTick, false);
    /* NOTE: IVG6 interrupt is not enabled at this point,
     * will be enabled later by calling cyanSchedulerStart(). */
}

void cyanRegisterInterruptHandler(uint8_t ivg, InterruptHandler handler, bool nesting)
{
    /* get event vector table base  */
    volatile uint32_t *evt = ((volatile uint32_t*)EVENT_VECTOR_TABLE_ADDR);

    /* register corresponding vector */
    if(nesting)
    {
        evt[ivg] = (uint32_t)&cyanNestingISR;
    }
    else
    {
        evt[ivg] = (uint32_t)&cyanNonNestingISR;
    }

    handlers[ivg] = handler;
}

L1_CODE void cyanInterruptHandler(void)
{
    uint32_t status, mask = 1;
    uint8_t i;

    status = *pIPEND & IPEND_BIT_4_MASK;

    /* test each interrupt */
    for (i = 0; i < IVG_NUM; i++)
    {
        /* interrupt did happen on this IVG number */
        if ((1 << i) == (status & mask))
        {
            /* it has a handler */
            if(handlers[i] != NULL)
            {
                /* call the handler */
                handlers[i]();
            }
        }

        mask <<=1;
    }
}

stack_t *cyanTaskStackInit(
    void (*task)(int, const char *[]),
    Arguments *args,
    stack_t *stackTop
) {
    stack_t *stk;
    uint8_t i;
    
    /* every task should has argument list */
    kassert(args);

    stk = stackTop;
    
    /* 3 words - R0 R1 R2 */
    stk -= 3;

    /* since args is a vector of char *, we can simply pull out its
     * buffer(data) and size, then pass size to R0 - first argument
     * and data to R1 - second argument.
     * then we have something like: ...(int argc, char *argv[])
     */

    /* R0 - first argument: argc */
    *--stk = (stack_t) args->size;

    /* P1 - value irrelevant */
    *--stk = (stack_t) 0;

    /* task return address */
    *--stk = (stack_t) cyanTaskExit;

    /* R1 - second argument: argv */
    *--stk = (stack_t) args->data;

    /* R2 - third argument: value irrelevant */
    *--stk = (stack_t) args;

    /* P0 - value irrelevant */
    *--stk = (stack_t) 0;

    /* P2 - value irrelevant */
    *--stk = (stack_t) 0;

    /* ASTAT - caller's ASTAT value - value irrelevant */
    *--stk = (stack_t) 0;

    /* RETI value - pushing the start address of the task */
    *--stk = (stack_t) task;

    for (i = 35; i > 0; i--)
    {
        /* remaining reg values - R7:3, P5:3,          */
        /* 4 words of A1:0(.W,.X), LT0, LT1,           */
        /* LC0, LC1, LB0, LB1,I3:0, M3:0, L3:0, B3:0,  */
        /* all values irrelevant                       */

        *--stk = (stack_t) 0;
    }
    
    return ((stack_t *)stk);
}
