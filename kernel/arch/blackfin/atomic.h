/*
 * Copyright (c) 2013, 2015-2016 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef ATOMIC_H__
#define ATOMIC_H__

#include <kernel/arch/blackfin/asm/system.h>

typedef struct
{
    int v;
} atomic_t;

/* initialize an atomic type */
#define ATOMIC_INIT(i)              { (i) }

/* read from an atomic type */
#define atomicRead(atomic)          ((atomic)->v)

/* set an atomic type */
#define atomicSet(atomic, i)        (((atomic)->v) = i)

/**
 *  Add an integer to an atomic type
 *
 *  @param i      An integer
 *  @param atomic A pointer to an atomic type
 */
static inline void atomicAdd(int i, atomic_t *atomic)
{
    int flags;
    local_irq_save(flags);
    {
        atomic->v += i;
    }
    local_irq_restore(flags);
}

/**
 *  Subtract an integer from an atomic type
 *
 *  @param i      An integer
 *  @param atomic A pointer to an atomic type
 */
static inline void atomicSub(int i, atomic_t *atomic)
{
    int flags;
    local_irq_save(flags);
    {
        atomic->v -= i;
    }
    local_irq_restore(flags);
}

/**
 *  Add an integer to an atomic type and return its value
 *
 *  @param i      An integer
 *  @param atomic A pointer to an atomic type
 *
 *  @return Value of the atomic type after being added
 */
static inline int atomicAddReturn(int i, atomic_t *atomic)
{
    int __temp = 0;
    
    int flags;
    local_irq_save(flags);
    {
        atomic->v += i;
        __temp = atomic->v;
    }
    local_irq_restore(flags);
    
    return __temp;
}

/**
 *  Subtract an integer to an atomic type and return its value
 *
 *  @param i      An integer
 *  @param atomic A pointer to an atomic type
 *
 *  @return Value of the atomic type after being subtracted
 */
static inline int atomicSubReturn(int i, atomic_t *atomic)
{
    int __temp = 0;
    
    int flags;
    local_irq_save(flags);
    {
        atomic->v -= i;
        __temp = atomic->v;
    }
    local_irq_restore(flags);
    
    return __temp;
}

/**
 *  Increment the value of an atomic type
 *
 *  @param atomic A pointer to an atomic type
 */
static inline void atomicInc(atomic_t *atomic)
{
    int flags;
    local_irq_save(flags);
    {
        atomic->v++;
    }
    local_irq_restore(flags);
}

/**
 *  Increment the value of an atomic type and return its value before
 *  being incremented
 *
 *  @param atomic A pointer to an atomic type
 *
 *  @return Value before being incremented
 */
static inline int atomicIncReturn(atomic_t *atomic)
{
    int ret = 0;

    int flags;
    local_irq_save(flags);
    {
        ret = atomic->v++;
    }
    local_irq_restore(flags);

    return ret;
}

/**
 *  Decrement the value of an atomic type
 *
 *  @param atomic A pointer to an atomic type
 */
static inline void atomicDec(atomic_t *atomic)
{
    int flags;
    local_irq_save(flags);
    {
        atomic->v--;
    }
    local_irq_restore(flags);
}

/**
 *  Compare two atomic types
 *
 *  @param a First atomic type
 *  @param b Second atomic type
 *
 *  @return 1 if they equal, 0 otherwise
 */
static inline int atomicCompare(atomic_t *a, atomic_t *b)
{
    int ret = 0;

    int flags;
    local_irq_save(flags);
    {
        ret = a->v == b->v ? 1 : 0;
    }
    local_irq_restore(flags);

    return ret;
}

/**
 *  Compare an atomic value to an numerical value
 *
 *  @param a A pointer to an atomic type
 *  @param b A value
 *
 *  @return 1 if they equal, 0 otherwise
 */
static inline int atomicCompareValue(atomic_t *a, int b)
{
    int ret = 0;

    int flags;
    local_irq_save(flags);
    {
        ret = a->v == b ? 1 : 0;
    }
    local_irq_restore(flags);

    return ret;
}

#endif /* ATOMIC_H__ */
