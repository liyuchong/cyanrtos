/*
 * Copyright (c) 2013, 2015-2016 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef CPU_H__
#define CPU_H__

#if(defined(_LANGUAGE_ASM) || defined(_LANGUAGE_C))
#   define  NESTED          1
#   define  NOT_NESTED      0
#endif 

/* only when being compiled using a C compiler */
#if defined(_LANGUAGE_C) 

#ifndef CYAN_RTOS_CPU_NAME
#   define CYAN_RTOS_CPU_NAME "Analog Devices ADSP-BF533"
#endif /* CYAN_RTOS_CPU_NAME */

#ifndef CYAN_TASK
#	define CYAN_TASK
#endif /* CYAN_TASK */

#define LITTLE_ENDIAN

/**
 *  Compiler-specific implementation
 */

#ifndef likely
#	define likely(x)   (x)
#endif /* likely */

#ifndef unlikely
#	define unlikely(x) (x)
#endif /* unlikely */

/* sometimes we need to force the compiler to put our code in L1 SRAM */
#ifndef L1_CODE
#	define L1_CODE section("L1_code")
#endif /* L1_CODE */

/* stdint definitions */
#include <kernel/arch/blackfin/intdef.h>

#include <cdefBF533.h>
#include <stddef.h>

/* stack entry */
typedef uint32_t stack_t;

/* Pointer to a function which returns void and has no  argument */
typedef void (* InterruptHandler)(void);

/* hardware related constants */
/* total IVG number */
#define IVG_NUM 16

/* 16 Interrupts vector number (0 to 15)*/
#define IVG0     0
#define IVG1     1
#define IVG2     2
#define IVG3     3
#define IVG4     4
#define IVG5     5
#define IVG6     6
#define IVG7     7
#define IVG8     8
#define IVG9     9
#define IVG10   10
#define IVG11   11
#define IVG12   12
#define IVG13   13
#define IVG14   14
#define IVG15   15

/* Event vector table base address
 * Reference: ADSP-BF533 Blackfin Processor Hardware Reference
 * Page 4-38
 * Table 4-9. Core Event Vector Table
 */
#define EVENT_VECTOR_TABLE_ADDR  0xFFE02000

/* Page 4-36; Interrupt Pending Register */
#define IPEND                    0xFFE02108
#define IPEND_BIT_4_MASK         0xFFFFFFEF
#define pIPEND                   ((volatile unsigned long *)IPEND)

#define CONTEXT_SWITCH_IRQ_ENABLED (*pIMASK & 0x4000)

/* raise IVG14(trap) -> context switch */
#define CYAN_SWITCH_CONTEXT_NOW()  asm("raise 14;")

/* interrupt handler functions */
static InterruptHandler handlers[IVG_NUM] = { NULL };

/**
 *  context.asm
 *  Context switch routine
 */
extern void cyanContextSwitch(void);

/**
 *  context.asm
 *  Context switch routine from an ISR
 */
extern void cyanContextSwitchISR(void);

/**
 *  interrupt.asm
 *  Non-nesting ISR entry routine
 */
extern void cyanNonNestingISR(void);

/**
 *  interrupt.asm
 *  Nesting ISR entry routine
 */
extern void cyanNestingISR(void);

#endif   /* _LANGUAGE_C */

#endif /* CPU_H__ */
