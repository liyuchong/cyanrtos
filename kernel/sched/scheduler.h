/*
 * Copyright (c) 2013, 2015-2016 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef CYAN_SCHEDULER_H__
#define CYAN_SCHEDULER_H__

#include <kernel/sys/sys.h>
#include <kernel/sched/queue_ops.h>
#include <kernel/arch/blackfin/cpu.h>

/**
 *  Initialize a given scheduler
 *
 *  @param sch           a pointer to the scheduler
 *  @param tickFrequency scheduler tick period in Hz
 *
 *  @return the input pointer to the scheduler
 */
CyanScheduler *cyanSchInit(CyanScheduler *sch, const uint16_t tickFrequency);

/**
 *  Insert a new task to a given queue
 *
 *  @param sch   Scheduler
 *  @param queue Queue
 *  @param task  New task
 */
void cyanSchTaskInsert(CyanScheduler *sch, CyanTcbQueue *queue, CyanTCB *task, bool resched);

/**
 *  Scheduler system tick
 *
 *  @param sch  Scheduler
 */
void cyanSchTick(CyanScheduler *sch);

/**
 *  Put a given task into sleep state by a given tick count
 *
 *  @param sch  Scheduler
 *  @param task Task to be put into sleep state
 *  @param tick Tick count
 */
void cyanSchTaskSleep(CyanScheduler *sch, CyanTCB *task, uint16_t tick);

/**
 *  Put a given task into stuck state
 *
 *  @param sch  Scheduler
 *  @param task Task to be put into stuck state
 */
void cyanSchTaskSuspend(CyanScheduler *sch, CyanTCB *task);

/**
 *  Resume a given task from stuck state
 *
 *  @param sch  Scheduler
 *  @param task Task to be resumed
 */
void cyanSchTaskResume(CyanScheduler *sch, CyanTCB *const task);

int cyanSchTaskPrioritySet(CyanScheduler *sch, CyanTCB *task, uint8_t priority);

/**
 *  Select a ready task and set the scheduler's run pointer to that task
 *
 *  @param sch Scheduler
 */
void cyanSchSelectTask(CyanScheduler *sch);

/**
 *  Perform a rescheduling:
 *  	1. select a ready task
 *  	2. run that task
 *
 *  Note that this function can be called with IRQ enabled or disabled.
 *  If IRQ is enabled, it selects a task and run it(a context switch).
 *  If IRQ is disabled, it only modifies the state of
 *  the scheduler(select task and set pointer), context switch
 *  will not be performed.
 *
 *  @param sch Scheduler
 */
static inline void cyanSchReschedule(CyanScheduler *sch)
{
    cyanSchSelectTask(sch);
    cyanContextSwitchTo(sch->run);
}

void cyanSchWait(CyanScheduler *sch, CyanTCB *tcb);

/**
 *  Reschedule - select a task from ready queue and switch to that task
 */
#define reschedule()                        \
do {                                        \
	cyanSchReschedule(&sched);				\
} while(0)

#endif /* CYAN_SCHEDULER_H__ */
