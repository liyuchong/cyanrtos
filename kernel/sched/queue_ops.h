/*
 * Copyright (c) 2013, 2015-2016 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef CYAN_SCHEDULER_UTILS_H__
#define CYAN_SCHEDULER_UTILS_H__

#include <kernel/sys/rtos.h>
#include <kernel/sys/queue.h>
#include <kernel/mt/lock.h>

/**
 *  Make a task ready to be run.
 *  This function resets a TCB's round count
 *  and assign it to the ready pointer.
 *
 *  NOTE: This function must be operated in critical section,
 *  it assumes that cyanDisableIRQ() has been called.
 *
 *  @param task Task TCB
 */
static inline void cyanSchMakeTaskReady(CyanTCB *const task)
{
    /* clear round count */
    task->roundCount = 0;

    /* assign to ready TCB pointer */
    cyanTcbReady = task;
}

/**
 *  Insert a task after the tail of a task queue
 *
 *  @param queue Task queue
 *  @param task  Task
 */
static inline void cyanSchQueueInsertTail_nl(CyanTcbQueue *queue, CyanTCB *task)
{
    kassert(!task->queue);

    STAILQ_INSERT_TAIL(&queue->q, task, next);
    task->queue = queue;
}

/* locked operation */
#define cyanSchQueueInsertTail(_queue, _task)               \
    do { spinLock(&queue->lock);                            \
        cyanSchQueueInsertTail_nl(_queue, _task);           \
        spinUnlock(&queue->lock);                           \
    } while(0)

/**
 *  Insert a task in front of the head of a task queue
 *
 *  @param queue Task queue
 *  @param task  Task
 */
static inline void cyanSchQueueInsertHead_nl(CyanTcbQueue *queue, CyanTCB *task)
{
    kassert(!task->queue);

    STAILQ_INSERT_HEAD(&queue->q, task, next);
    task->queue = queue;
}

/* locked operation */
#define cyanSchQueueInsertHead(_queue, _task)           \
    do { spinLock(&queue->lock);                        \
        cyanSchQueueInsertHead_nl(_queue, _task);       \
        spinUnlock(&queue->lock);                       \
    } while(0)

/**
 *  Insert a task after a given item in a task queue
 *
 *  @param queue Task queue
 *  @param item  Item which the task will be inserted after
 *  @param task  Task
 */
static inline void cyanSchQueueInsertAfter_nl(
        CyanTcbQueue *queue, CyanTCB *item, CyanTCB *task
) {
    kassert(!task->queue);

    STAILQ_INSERT_AFTER(&queue->q, item, task, next);
    task->queue = queue;
}

/* locked operation */
#define cyanSchQueueInsertAfter(_queue, _item, _task)       \
    do { spinLock(&queue->lock);                            \
        cyanSchQueueInsertAfter_nl(_queue, _item, _task);   \
        spinUnlock(&queue->lock);                           \
    } while(0)

/**
 *  Remove a task from its queue
 *
 *  @param task Task
 *
 *  @return Pointer to the input task
 */
static inline CyanTCB *cyanSchQueueRemove(CyanTCB *task)
{
    /* task should belongs to some queue */
    if(!task->queue)
    {
        uprintf("task: %s\r\n", task->name);
        panic("trying to remove a task from nothing");
    }

    /* remove it from its original queue */
    spinLock(&task->queue->lock); {
        STAILQ_REMOVE(&task->queue->q, task, CyanTCB, next);
    } spinUnlock(&task->queue->lock);

    /* set its current queue to nothing */
    task->queue = NULL;

    return task;
}

/**
 *  Switch to a given task.
 *
 *  @param task Task TCB
 */
L1_CODE static inline void cyanContextSwitchTo(CyanTCB *const ready)
{
    if(cyanTcbReady != ready)
    {
        cyanTcbReady = ready;

        if(running)
        {
            ready->stat.contextSwitch++;

            CYAN_SWITCH_CONTEXT_NOW();
        }
    }
}

#endif /* CYAN_SCHEDULER_UTILS_H__ */
