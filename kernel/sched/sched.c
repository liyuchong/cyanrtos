/*
 * Copyright (c) 2013, 2015-2016 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <kernel/sched/scheduler.h>
#include <kernel/sched/queue_ops.h>

/*
 * Priority based preemptive(+ RR) scheduler
 * -----------------------------------------
 *
 *  the scheduler performs a context switch if one of the following occurs:
 *
 *  1. A task is inserted into the ready queue, and its priority is higher
 *     than the currently running task.
 *  2. A task which is currently running, being removed from the ready queue.
 *  3. (RR) If there are two or more tasks share the same highest priority,
 *     and the currently running task's round ends.
 *
 *  hence, besides the system tick routine cyanSchTick() which checks if a
 *    context switch is needed actively, calling one of the following
 *    functions which calls cyanContextSwitchTo()
 *    could result in a context switch:
 *
 *      cyanSchTaskPrioritySet()
 *      cyanSchTaskSleep()
 *      cyanSchTaskStuck()
 *      cyanSchTaskResume()
 */

CyanScheduler *cyanSchInit(CyanScheduler *sch, const uint16_t tickFrequency)
{
    kassert(sch);

    /* no task is currently running */
    sch->run = NULL;

    sch->tickFrequency = tickFrequency;
    sch->tick = 0;
    sch->priodTick = 0;
    sch->readyTaskCount = 0;

    /* initialize ready queue */
    STAILQ_INIT(&sch->ready.q);

    /* initialize sleep queue */
    STAILQ_INIT(&sch->sleep.q);

    /* initialize stuck queue */
    STAILQ_INIT(&sch->stuck.q);

    return sch;
}

/**
 *  Time tick, do a sleep counter increment to each task in the
 *  sleep queue and test which of them need to be resumed. Remove them
 *  from the sleep queue and insert them into ready queue, if any.
 */
L1_CODE static inline void cyanSchTimerTick(CyanScheduler *sch)
{
    /* this code is inside critical section, so the iteration of sleep
     * queue does not require a spin lock. */

    CyanTCB *task;
    STAILQ_FOREACH(task, &sch->sleep.q, next)
    {
        task->sleep--;

        /* time's up */
        if(task->sleep == 0)
        {
            /* remove task from sleep queue, put it back to ready queue. */
            cyanSchQueueRemove(task);

            /* now insert to ready queue */
            cyanSchTaskInsert(sch, &sch->ready, task, true);
        }
    }
}

/**
 *  Periodic scheduler tick
 *  The scheduler update statistic counters, then call timer tick,
 *  finally select a task and switch to task task.
 */
L1_CODE void cyanSchTick(CyanScheduler *sch)
{
    int flags;
    local_irq_save(flags);
    {
        sch->tick++;
        sch->priodTick++;

        sch->run->stat.cpuTime++;
        sch->run->stat.periodTime++;

        cyanSchTimerTick(sch);

        /* select a ready task with highest priority */
        cyanSchSelectTask(sch);
    }
    local_irq_restore(flags);

    cyanContextSwitchTo(sch->run);
}

/**
 *  Select a task from the ready queue to run. Since the ready queue is
 *  guaranteed to be sorted, we can just select the first task in the queue.
 *  The scheduler allows duplicated priority, so if we have more than 1
 *  ready task shares the same priority, the scheduler runs these tasks in
 *  RR mode. Timeslice is assigned during the creation of each task.
 */
L1_CODE void cyanSchSelectTask(CyanScheduler *sch)
{
    kassert(sch);

    int flags;
    local_irq_save(flags);

    spinLock(&sch->ready.lock);

    /* should have at least one task to run, issue a kernel panic if there's
     * no task in ready queue */
    if(!STAILQ_FIRST(&sch->ready.q))
    {
        panic("no task to run, "
              "should have at least one task in ready queue.\r\n"
              "(where is `idled'?) ");
    }

    /* get the highest priority in the queue */
    const uint8_t priority = STAILQ_FIRST(&sch->ready.q)->priority;

    /* count number of ready tasks */
    sch->readyTaskCount = 0;

    CyanTCB *tcb;
    STAILQ_FOREACH(tcb, &sch->ready.q, next)
    {
        if(priority == tcb->priority)
        {
            sch->readyTaskCount++;
        }
        else
        {
            /* stop once we hit a task which has different priority than
             * the selected task */
            break;
        }
    }

    /* should have at least 1 ready task in the ready queue */
    kassert(sch->readyTaskCount > 0);

    spinUnlock(&sch->ready.lock);

    /* set run pointer to the first task in the ready queue(highest priority) */
    sch->run = STAILQ_FIRST(&sch->ready.q);

    /* RR
     * Algorithm: if we have more than 1 ready tasks with the same priority,
     * run the first task in the ready queue, when it's round ends, put it to
     * the tail of the tasks which have the same priority */

    /* more than 1 task have the same highest priority */
    if(sch->readyTaskCount > 1)
    {
        CyanTCB *current = sch->run;

        current->roundCount++;

        /* time's up */
        if(current->roundCount >= current->round)
        {
            current->roundCount = 0;

            spinLock(&sch->ready.lock);

            CyanTCB *tcb;
            STAILQ_FOREACH(tcb, &sch->ready.q, next)
            {
                if(STAILQ_NEXT(tcb, next) &&
                   current->priority != STAILQ_NEXT(tcb, next)->priority)
                {
                    STAILQ_REMOVE(&sch->ready.q, current, CyanTCB, next);
                    STAILQ_INSERT_AFTER(&sch->ready.q, tcb, current, next);

                    break;
                }
            }

            spinUnlock(&sch->ready.lock);
        }
    }

    local_irq_restore(flags);
}

L1_CODE void cyanSchTaskInsert(CyanScheduler *sch, CyanTcbQueue *queue, CyanTCB *task, bool resched)
{
    kassert(sch);
    kassert(task);

    CyanTCB *tcb;

    /* lock queue spin lock, all queue operations after this must be
     * non-locked operations(functions with _nl suffix). */
    spinLock(&queue->lock);

    /* if queue is empty, simply insert the task into
     * the tail of the queue */
    if(STAILQ_EMPTY(&queue->q))
    {
        cyanSchQueueInsertTail_nl(queue, task);

        goto __cyan_insert_task_end;
    }

    /* the following code tests if the priority of the new task is less
     * than the first task in the queue, of if the priority of the new
     * task is greater than the last task in the queue. */

    /* if the priority of the first task in the queue is greater than
     * the task's priority, insert the task to the head of the queue. */
    if(STAILQ_FIRST(&queue->q)->priority >= task->priority)
    {
        cyanSchQueueInsertHead_nl(queue, task);

        goto __cyan_insert_task_end;
    }

    /* otherwise, the task should be placed between head and tail,
     * compare priority of each item in the queue to find its position. */
    STAILQ_FOREACH(tcb, &queue->q, next)
    {
        /* reach the end of the queue */
        if(!STAILQ_NEXT(tcb, next))
        {
            cyanSchQueueInsertTail_nl(queue, task);

            goto __cyan_insert_task_end;
        }
        else
        {
            /* find a position such that the task's priority
             * is less or equal to current tcb's priority, but greater
             * or equal to the tcb's priority.
             *
             *      condition: p1 <= px <= p2
             *
             *  low -> high (value)
             *  priority | ... |  p1  |  p2  | ...
             *                     ^  ^   ^
             *                     |  |   |
             *                     |  |   `- tcb->next
             *                tcb -   `- insertion point(px)
             */
            if(tcb->priority <= task->priority &&
               STAILQ_NEXT(tcb, next)->priority >= task->priority)
            {
                /* insert the new task after tcb */
                cyanSchQueueInsertAfter_nl(queue, tcb, task);

                goto __cyan_insert_task_end;
            }
        }
    }

    /* should never get here */
    panic("...?");

__cyan_insert_task_end:

    /* unlock queue spin lock */
    spinUnlock(&queue->lock);

    /* select a ready task */
    if(resched)
    {
        cyanSchSelectTask(sch);
    }

    /* if the system is not running, set the current TCB pointer
     * to the selected task */
    if(!running)
    {
        cyanTcbCurrent = sch->run;
        cyanTcbReady = sch->run;
    }
}

L1_CODE int cyanSchTaskPrioritySet(CyanScheduler *sch, CyanTCB *task, uint8_t priority)
{
    kassert(sch);
    kassert(task);

    CyanTcbQueue *queue;

    cyanDisableIRQ();
    {
        /* get the task's current queue */
         queue = task->queue;

        kassert(queue);

        if(!queue)
        {
            panic("task seems belongs to nothing");
        }

        /* remove task from its queue and re-insert it to the queue */
        cyanSchQueueRemove(task);

        /* set task's new priority */
        task->priority = priority;
    }
    cyanEnableIRQ();

    /* re-insert it into its original queue */
    cyanSchTaskInsert(sch, queue, task, true);
    cyanSchReschedule(sch);

    return 0;
}

L1_CODE void cyanSchTaskSleep(CyanScheduler *sch, CyanTCB *task, uint16_t tick)
{
    cyanDisableIRQ();
    {
        kassert(sch);
        kassert(task);

        task->sleep = tick;
        cyanSchQueueRemove(task);

        cyanSchTaskInsert(sch, &sch->sleep, task, true);
    }
    cyanEnableIRQ();

    cyanSchReschedule(sch);
}

L1_CODE void cyanSchTaskSuspend(CyanScheduler *sch, CyanTCB *task)
{
    kassert(sch && task);

    /* task should not in stuck queue */
    if(task->queue == &sch->stuck)
    {
        panic("trying to suspend a task which is currently in stuck queue");
    }

    taskWaitReferenceInc(task);

    int flags;

    local_irq_save(flags);
    {
        /* remove task from their original queue */
        cyanSchQueueRemove(task);

        /* insert it into stuck queue */
        cyanSchTaskInsert(sch, &sch->stuck, task, true);
    }
    local_irq_restore(flags);

    /* switch to the task which has the highest priority in the queue */
    cyanSchSelectTask(sch);
    cyanContextSwitchTo(sch->run);
}

L1_CODE void cyanSchTaskResume(CyanScheduler *sch, CyanTCB *const task)
{
    kassert(sch);
    kassert(task);

    /* check if the task is in stuck state, issue a kernel panic if it isn't */
    if(!task->queue)
    {
        printk("task: %s(%x) queue: 0x%p\r\n",
                task->name, task->queue, task->pid);
        printk("ready: 0x%p, sleep: 0x%p, stuck: 0x%p\n",
                &sch->ready, &sch->sleep, &sch->stuck);
        panic("trying to resume a task which is not in stuck queue");
    }

    /* decrement suspend reference counter */
    taskWaitReferenceDec(task);

    /* only resume the task when there's no more suspension request */
    if(atomicCompareValue(&task->suspendCounter, 0))
    {
        int flags;

        local_irq_save(flags);
        {
            /* remove task from their original queue */
            cyanSchQueueRemove(task);

            /* insert it into ready queue */
            cyanSchTaskInsert(sch, &sch->ready, task, true);
        }
        local_irq_restore(flags);

        cyanSchReschedule(sch);
    }
}

L1_CODE void cyanSchWait(CyanScheduler *sch, CyanTCB *tcb)
{
    /* remove task from its original queue */
    int flags;

    local_irq_save(flags);
    {
        kassert(tcb);

        CyanTCB *run = sch->run;
        SV_PUSH_BACK(&tcb->waitList, run);

        run->waitingFor = tcb;

        cyanSchTaskSuspend(sch, run);
    }
    local_irq_restore(flags);

    cyanSchReschedule(sch);
}
