/*
 * Copyright (c) 2013, 2015-2016 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <kernel/sys/rtos.h>
#include <kernel/sched/scheduler.h>
#include <kernel/sched/queue_ops.h>

CyanTCB *cyanTaskCreate(
        void (*task)(int, const char *[]), Arguments *arg, uint32_t stackSize,
        char *name, uint8_t priority, uint16_t round,
        bool schedNow
) {
    /* allocate TCB */
    CyanTCB *tcb = malloc_mt(sizeof(CyanTCB));

    /* allocate stack */
    stack_t *stack = malloc_mt(sizeof(stack_t) * stackSize);

    if(!stack || !tcb)
    {
        panic("out of memory");
    }

    /* stdin & stdout
     * The TCB has a vector of message queue which stores stdin and stdout
     * of the specific task */
    SV_INIT(&tcb->fds, 2, CircularBuffer *);
    SV_PUSH_BACK(&tcb->fds, ttyRoot->in);
    SV_PUSH_BACK(&tcb->fds, ttyRoot->out);

    /* every task should has an argument list, which is a
     * vector of char pointer, and the size of the vector and the
     * actual pointer will be passed to the task. so if our argument list
     * is NULL, then we create one. all allocated memory for the argument
     * list will be released upon task return. */
    if(!arg)
    {
        /* allocate memory for argument list */
        Arguments *arguments = malloc_mt(sizeof(VECTOR(char *)));

        if(!arguments)
        {
            panic("out of memory");
        }

        SV_INIT(arguments, 2, char *);

        /* make a copy of task's name, will be freed upon task return */
        char *cmd = malloc_mt(CYAN_RTOS_TASK_NAME_LEN);

        if(!cmd)
        {
            panic("out of memory");
        }

        strncpy(cmd, name, CYAN_RTOS_TASK_NAME_LEN);
        SV_PUSH_BACK(arguments, cmd);

         tcb->args = arguments;
    }
    else
    {
         tcb->args = arg;
    }

    /* initialize the stack and TCB, then insert it into the scheduler */
    cyanTaskInit(
        tcb, task, tcb->args, stack, stackSize,
        name, priority, round, schedNow
    );

    /* reschedule */
    if(schedNow)
    {
        reschedule();
    }

    return tcb;
}

CyanTCB *cyanTaskInit(
        CyanTCB *tcb,
        void (*task)(int, const char *[]), Arguments *arg,
        stack_t *stack, size_t stackSize,
        char *name, const uint8_t priority, const uint16_t round,
        bool resched
) {
    /* assign PID */
    tcb->pid = pidMax++;

    tcb->tty = NULL;

    /* assign parent process */
    if(running)
    {
        tcb->parent = sched.run;

        // XXX: assign parent's stdin & stdout to the task
        tcb->tty = tcb->parent->tty;
    }
    else
    {
        tcb->parent = NULL;
    }

    /* allocate memory for task name and copy name */
    tcb->name = malloc_mt(CYAN_RTOS_TASK_NAME_LEN);

    if(!tcb->name)
    {
        panic("cannot create task: out of memory");
    }
    else
    {
        /* actually copy task's name */
        memset(tcb->name,  0,    CYAN_RTOS_TASK_NAME_LEN);
        strncpy(tcb->name, name, CYAN_RTOS_TASK_NAME_LEN);
    }

    /* pointer to the code piece of the task */
    tcb->main = task;

    /* reset fields,
     * the task currently belongs to no queue;
     * the task is not waiting for any other task;
     * the task has nothing to do while it's being killed yet. */
    tcb->queue      = NULL;
    tcb->waitingFor = NULL;
    tcb->exiting    = NULL;

    /* we don't know the free space of the task's stack yet, so set it -1 */
    tcb->stackFree = -1;

    /* here are some statistic stuff, let's set them to 0. */
    tcb->stat.cpuTime       = 0;
    tcb->stat.periodTime    = 0;
    tcb->stat.contextSwitch = 0;
    tcb->stat.cpuUsage      = 0;

    tcb->roundCount = 0;
    tcb->sleep      = 0;

    /* initialize the task's waiting list, different from the 'waitingFor'
     * field, these are the tasks which are waiting for THIS task, instead
     * of this task waiting for ANOTHER task. */
    SV_INIT(&tcb->waitList, 2, CyanTCB *);

    /* signal mechanism stuff, we have locks and signal/handlers,
     * initialize them. */
    spinLockInit(&tcb->signalHandlersLock);
    spinLockInit(&tcb->signalsLock);
    STAILQ_INIT(&tcb->signalHandlers);
    STAILQ_INIT(&tcb->signals);

    /* the suspend counter is of type atomic_t, set it to 0. */
    atomicSet(&tcb->suspendCounter, 0);

    /* now let's deal with the RR algorithm related field */
    /* convert millisecond to tick count */
    tcb->round = round * CYAN_RTOS_TICKS_PER_SEC / 1000;
    tcb->round = tcb->round <= 0 ? 1 : tcb->round;

    /* set task's priority */
    tcb->priority = priority;

    /* TCB's stack stuff */
    tcb->stackSize = stackSize;

    tcb->stackPtr = stack;
    memset(tcb->stackPtr, 0, sizeof(stack_t) * stackSize);

    /* initialize stack and assign it to TCB, notice that we are passing
     * the top of the stack to the stack initialization routine. */
    tcb->stack = cyanTaskStackInit(task, arg, stack + stackSize - 1);

    /* insert the task into the scheduler, and we are done */
    cyanSchTaskInsert(&sched, &sched.ready, tcb, resched);

    return tcb;
}

void cyanReleaseArgumentList(Arguments *args)
{
    for(int i = 0; i < SV_SIZE(args); i++)
    {
        free_mt(SV_AT(args, i));
    }

    SV_RELEASE(args);
    free_mt(args);
}

static void msgQueueRemove(CyanTCB *tcb)
{
    for(int i = 0; i < MSG_QUEUE_MAX_QUEUE; i++)
    {
        MsgQueue *queue = mqueueMan.queues[i];
        if(queue)
        {
            STACK_ERASE_VALUE(&queue->tcbs, void *, tcb);

            if(STACK_COUNT(&queue->tcbs) == 0)
            {
                // XXX: release memory

            }
        }
    }
}

/**
 *  Kill a given task:
 *      1. remove it from its queue(one of ready, sleep or stuck)
 *      2. un-register it from its message queue
 *      3. release its argument list memory
 *      4. release its stack
 *      5. release itself
 *
 *  Note: this is a critical section operation
 *
 *  @param tcb Task control block
 */
static inline void __kill(CyanTCB *tcb)
{
    /* remove the task from its queue */
    cyanSchQueueRemove(tcb);

    /* if the task has a registered message queue, remove that queue */
    msgQueueRemove(tcb);

    /* release parameter list */
    kassert(tcb->args);
    cyanReleaseArgumentList(tcb->args);

    SV_RELEASE(&tcb->fds);

    /* release memory */
    free_mt(tcb->stackPtr);
    free_mt(tcb->name);
    free_mt(tcb);
}

void cyanKillTask(CyanTCB *tcb)
{
    int flags;
    int exitingRetCode = 0;

    /* enter critical section */
    local_irq_save(flags);
    {
        kassert(tcb);

        if(!tcb->queue)
        {
            printk("TCB: %s\r\n", tcb->name);
            panic("where is TCB's queue? ");
        }

        /* call task's exiting function first */
        if(tcb->exiting)
        {
            exitingRetCode =
            tcb->exiting(tcb->args->size, (const char **)tcb->args->data);

            /* kill task aborted by the task, abort routine */
            if(exitingRetCode != 0)
            {
                goto __kill_task_aborted;
            }
        }

        // XXX: remove task from any fifo waiting list

        /* if the task is waiting for another task, remove the task from
         * the other task's wait list. */
        if(tcb->waitingFor)
        {
            VECTOR(CyanTCB *) *waitList = (void *)&tcb->waitingFor->waitList;

            for(int i = 0; i < SV_SIZE(waitList); i++)
            {
                if(tcb == SV_AT(waitList, i))
                {
                    SV_ERASE_AT(waitList, i);
                }
            }
        }

        /* let's resume waiting tasks, since the IRQ is disabled, no context
         * switch shall happen, this allows us to finish the kill routine */
        for(int i = 0; i < SV_SIZE(&tcb->waitList); i++)
        {
            SV_AT(&tcb->waitList, i)->waitingFor = NULL;
            cyanSchTaskResume(&sched, SV_AT(&tcb->waitList, i));
        }

        SV_RELEASE(&tcb->waitList);

        /* do the actual job */
        __kill(tcb);

        /* select next task */
        cyanSchSelectTask(&sched);
    }
    /* exit critical section */
    local_irq_restore(flags);

    cyanContextSwitchTo(sched.run);

    return;

__kill_task_aborted:

    printk("%s(%d): kill: task returns %d, kill routine aborted\r\n",
    	   tcb->name, tcb->pid, exitingRetCode);
    local_irq_restore(flags);

    return;
}

void cyanTaskExit(void)
{
    /* suicide */
    cyanKillTask(sched.run);
}
