/*
 * Copyright (c) 2013, 2015-2016 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef IN_H__
#define IN_H__

#include <kernel/arch/blackfin/intdef.h>
#include <kernel/arch/blackfin/cpu.h>
#include <kernel/sys/ipc.h>

#if defined(BIG_ENDIAN) && !defined(LITTLE_ENDIAN)
#   define htons(A) (A)
#   define htonl(A) (A)
#   define ntohs(A) (A)
#   define ntohl(A) (A)
#   elif defined(LITTLE_ENDIAN) && !defined(BIG_ENDIAN)
#   define htons(A)                         \
        ((((uint16_t)(A) & 0xff00) >> 8) |  \
        (((uint16_t)(A) & 0x00ff) << 8))

#   define htonl(A)                                 \
        ((((uint32_t)(A) & 0xff000000) >> 24) |     \
        (((uint32_t)(A) & 0x00ff0000)  >> 8)  |     \
        (((uint32_t)(A) & 0x0000ff00)  << 8)  |     \
        (((uint32_t)(A) & 0x000000ff)  << 24))
#   define ntohs htons
#   define ntohl htohl
#else
#   error "BIG_ENDIAN? LITTLE_ENDIAN?"
#endif

#define INADDR_NONE     0xffffffff      /* -1 return */

typedef uint32_t    in_addr_t;  /* base type for internet address */

/*
 * Internet address (a structure for historical reasons)
 */
struct in_addr {
    in_addr_t s_addr;
};

extern CircularBuffer inetIn;

uint32_t inet_addr(const char *cp);

int inet_aton(const char *cp, struct in_addr *addr);

int8_t hw_addr(const char *str, uint8_t *hw);

#endif /* IN_H__ */
