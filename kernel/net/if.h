/*
 * Copyright (c) 2013, 2015-2016 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef IF_H__
#define IF_H__

#include <kernel/arch/blackfin/intdef.h>
#include <kernel/sys/vector.h>
#include <kernel/sys/utils.h>

enum
{
	INET_DUPLEX_UNKNOWN = 0,
	INET_DUPLEX_HALF = 1,
	INET_DUPLEX_FULL = 2,
};

enum
{
	INET_SPD_UNKNOWN = 0,
	INET_SPD_10MTX = 1,
	INET_SPD_100MTX = 2,
};

typedef struct EtherInterface
{
    char ifname[16];

    uint8_t hwaddr[6];

    uint32_t ipaddr;
    uint32_t netmask;
    uint32_t gateway;

    size_t tx, rx;
    size_t tx_packets, rx_packets;

    int duplex;
    int speed;

} EtherInterface;

typedef VECTOR(EtherInterface) EtherInterfaces;

extern EtherInterfaces interfaces;

void interfacesInit(void);

int findInterface(const char *ifname);

#endif /* IF_H__ */
