/*
 * Copyright (c) 2013, 2015-2016 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <kernel/net/if.h>
#include <kernel/net/in.h>

EtherInterfaces interfaces;

CircularBuffer inetIn;

void interfacesInit(void)
{
	SV_INIT(&interfaces, 2, EtherInterface);

	EtherInterface eif;

	strcpy(eif.ifname, "eth0");

	eif.speed = INET_SPD_UNKNOWN;
	eif.duplex = INET_DUPLEX_UNKNOWN;

	eif.ipaddr = inet_addr("10.0.1.93");
	eif.gateway = inet_addr("10.0.1.1");
	eif.netmask = inet_addr("255.255.255.0");

	eif.hwaddr[0] = 0x54;
	eif.hwaddr[1] = 0x2b;
	eif.hwaddr[2] = 0x2b;
	eif.hwaddr[3] = 0x2b;
	eif.hwaddr[4] = 0x2b;
	eif.hwaddr[5] = 0x2b;

	eif.tx = 0;
	eif.rx = 0;
	eif.tx_packets = 0;
	eif.rx_packets = 0;

	SV_PUSH_BACK(&interfaces, eif);

	circularBufferInit(&inetIn, 20480);
}
