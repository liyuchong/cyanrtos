/*
 * Copyright (c) 2013, 2015-2016 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <kernel/ipc/fifo.h>
#include <kernel/sys/sys.h>
#include <kernel/sched/scheduler.h>
#include <kernel/sched/queue_ops.h>
#include <kernel/arch/blackfin/cpu.h>

#include <string.h>

#define MIN(x, y) ((x) < (y) ? (x) : (y))

int circularBufferInit(CircularBuffer *fifo, size_t size)
{
    fifo->size = size;
    fifo->in = fifo->out = 0;

    fifo->buf = FIFO_MALLOC(size);

    if (!fifo->buf)
    {
        return -1;
    }

    spinLockInit(&fifo->lock);

    SV_INIT(&fifo->tcbs, 2, void *);

    return 0;
}

size_t circularBufferRead(CircularBuffer *fifo, uint8_t *buf, size_t len, int flag)
{
    int flags;
    local_irq_save(flags);

    /* FIFO is empty */
    if(circularBufferEmpty(fifo))
    {
        switch(flag)
        {
            case IPC_NOWAIT:
            {
                /* the FIFO is empty, and non-blocked read is specified, so
                 * enable IRQ and return 0 */
                local_irq_restore(flags);

                return 0;
            }

            case IPC_BLOCKED:
            {
                /* IPC_BLOCKED,
                 * block the task until there's a new message available */
                CyanTCB *tcb = sched.run;

                kassert(tcb->queue != &sched.stuck);

                /* push the caller task to the queue's stack */
                SV_PUSH_BACK(&fifo->tcbs, tcb);

                /* enable IRQ, ready for context switch */
                local_irq_restore(flags);

                /* context switch interrupt shall be enabled */
                kassert(CONTEXT_SWITCH_IRQ_ENABLED);

                /* block the function call */
                cyanSchTaskSuspend(&sched, tcb);

                /* now the blocked call is resumed, */
                /* should have message in the FIFO i.e. non-empty */
                kassert(!circularBufferEmpty(fifo));

                break;
            }

            default: panic("unknown flag");
        }
    }

    /* read FIFO */
    spinLock(&fifo->lock);
    {
        len = MIN(len, fifo->in - fifo->out);
        size_t l = MIN(len, fifo->size - (fifo->out & (fifo->size - 1)));

        memcpy(buf, fifo->buf + (fifo->out & (fifo->size - 1)), l);
        memcpy(buf + l, fifo->buf, len - l);

        /* move read pointer */
        fifo->out += len;
    }
    spinUnlock(&fifo->lock);

    /* done, enable IRQ and return bytes read */
    local_irq_restore(flags);

    return len;
}

size_t circularBufferWrite(CircularBuffer *fifo, uint8_t *buf, size_t len)
{
    int flags;
    local_irq_save(flags);

    spinLock(&fifo->lock);
    {
        len = MIN(len, fifo->size - fifo->in + fifo->out);
        size_t l = MIN(len, fifo->size - (fifo->in & (fifo->size - 1)));

        memcpy(fifo->buf + (fifo->in & (fifo->size - 1)), buf, l);
        memcpy(fifo->buf, buf + l, len - l);

        /* move write pointer */
        fifo->in += len;
    }
    spinUnlock(&fifo->lock);

    /* if the FIFO has a blocked function call */
    if(SV_SIZE(&fifo->tcbs) > 0)
    {
        /* get the top TCB, don't pop it yet */
        CyanTCB *tcb = SV_LAST_ELEMENT(&fifo->tcbs);

        /* this task must has been inserted into the stuck queue */
        if(tcb && tcb->queue == &sched.stuck)
        {
            cyanSchTaskResume(&sched, tcb);

            // XXX: make a SV_POP
            /* not we can pop it */
            SV_ERASE_AT(&fifo->tcbs, ((int)SV_SIZE(&fifo->tcbs)) - 1);
        }
        else
        {
            /* the task is not in stuck queue, could be caused by the task
             * being modified by other routines, also could happen when
             * the FIFO is full, yet the consumer has not read the FIFO.
             * there's nothing we can do either way, so just print a warning.
             */
            printk("[Warning] circularBufferWrite: "
                   "not in stuck queue: %s(%d) -> 0x%x\r\n",
                   tcb->name, tcb->pid, tcb->queue);
        }
    }

    local_irq_restore(flags);

    return len;
}
