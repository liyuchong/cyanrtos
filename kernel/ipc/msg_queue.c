/*
 * Copyright (c) 2013, 2015-2016 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <kernel/ipc/fifo.h>

#include <kernel/sys/sys.h>
#include <kernel/sched/scheduler.h>
#include <kernel/sched/queue_ops.h>

extern CyanScheduler sched;

MsgQueueManager *msgQueueManagerInit(MsgQueueManager *manager)
{
    /* initialize manager's spin lock */
    spinLockInit(&manager->lock);

    spinLock(&manager->lock);

    for(int i = 0; i < MSG_QUEUE_MAX_QUEUE; i++)
    {
        manager->queues[i] = NULL;
    }

    spinUnlock(&manager->lock);
    
    return manager;
}

void msgQueueManagerDestroy(MsgQueueManager *manager)
{
    spinLock(&manager->lock);

    for(int i = 0; i < MSG_QUEUE_MAX_QUEUE; i++)
    {
        if(manager->queues[i] != NULL)
        {
            /* free memory */
            FIFO_MFREE(manager->queues[i]);
            
            /* set to NULL */
            manager->queues[i] = NULL;
        }
    }

    spinUnlock(&manager->lock);
}

MsgQueue *msgQueueGet(
        MsgQueueManager *manager,
        const uint8_t key, const uint16_t flag
) {
    MsgQueue *queue;

    spinLock(&manager->lock); {
    	queue = manager->queues[key];
    } spinUnlock(&manager->lock);
    
    /* queue found, just return the queue */
    if(queue)
    {
        return queue;
    }
    
    /* queue not found, but flag is set to MSG_QUEUE_FIND, so do not create
     * a new one, just return NULL. */
    if(flag == MSG_QUEUE_FIND)
    {
        return NULL;
    }
    
    /* queue does not exist, create a new one */
    MsgQueue *new = FIFO_MALLOC(sizeof(MsgQueue));
    
    if(!new)
    {
        panic("out of memory");
    }
    
    /* copy id */
    new->id = key;
    new->len = 0;
    
    // XXX: yeah...
    STACK_INIT(&new->tcbs, void *, 16);

    /* initialize spin lock */
    spinLockInit(&new->lock);

    /* initialize queue structure */
    spinLock(&new->lock); {
        STAILQ_INIT(&new->queue);
    } spinUnlock(&new->lock);
    
    /* update queue manager */
    spinLock(&manager->lock); {
    	manager->queues[key] = new;
    } spinUnlock(&manager->lock);
    
    return new;
}

int msgQueueSend(MsgQueue *msgQueue, void *message, const int flag)
{
    MsgQueueNode *node = FIFO_MALLOC(sizeof(MsgQueueNode));
    
    if(!node)
    {
        /* memory allocation failed */
        panic("out of memory");
    }
    
    /* assign message */
    node->message = message;
    
    /* enqueue */
    switch (flag)
    {
        /* insert to the tail */
        case MSG_QUEUE_INSERT_NORMAL:
        {
            spinLock(&msgQueue->lock); {
                STAILQ_INSERT_TAIL(&msgQueue->queue, node, next);
            } spinUnlock(&msgQueue->lock);

            msgQueue->len++;

            break;
        }
            
        /* insert to the head */
        case MSG_QUEUE_INSERT_HEAD:
        {
            spinLock(&msgQueue->lock); {
                STAILQ_INSERT_HEAD(&msgQueue->queue, node, next);
            } spinUnlock(&msgQueue->lock);

            msgQueue->len++;

            break;
        }
            
        /* flag not recognized */
        default:
            goto __mq_send_error;
    }

    /* if the queue has a blocked function call */
    if(STACK_COUNT(&msgQueue->tcbs) > 0)
    {
        cyanDisableIRQ();
        {
            /* copy TCB pointer and set the TCB pointer in the queue to NULL */
        	CyanTCB *tcb = STACK_POP(&msgQueue->tcbs);

            /* already resumed */
            if(!tcb->queue)
            {
                goto __mq_send_already_resumed;
            }

            cyanSchTaskResume(&sched, tcb);
        }
        cyanEnableIRQ();
    }
    
__mq_send_done:
    return 0;

__mq_send_already_resumed:
	cyanEnableIRQ();
	return 0;

__mq_send_error:
    return -1;
}

/**
 *  Try to fetch message once, return message if there's a message,
 *  NULL otherwise
 *
 *  @param queue A pointer to a message queue
 *
 *  @return message
 */
static inline void *msgQueueFetchOnce(MsgQueue *queue)
{
    /* message queue node */
    MsgQueueNode *node;
    /* actual message on the node */
    void *msg = NULL;

    /* remove message from queue */
    spinLock(&queue->lock);
    {
        node = STAILQ_FIRST(&queue->queue);

        /* no message, unlock spin lock and return NULL */
        if(!node) goto __mq_fetch_no_message_unlock;

        STAILQ_REMOVE(&queue->queue, node, MsgQueueNode, next);
    }
    spinUnlock(&queue->lock);

    queue->len--;

    /* get payload */
    msg = node->message;

    /* release memory */
    FIFO_MFREE(node);
    node = NULL;

    /* return the message */
    return msg;

    /* only gets here when there's no message available */
__mq_fetch_no_message_unlock:

    spinUnlock(&queue->lock);
    return NULL;
}

void *msgQueueRecv(MsgQueue *queue, const int flag)
{
    MsgQueueNode *node;

    cyanDisableIRQ();

    /* get the first message in the queue */
    spinLock(&queue->lock); {
        node = STAILQ_FIRST(&queue->queue);
    } spinUnlock(&queue->lock);
    
    if(!node)
    {
        switch(flag)
        {
            case IPC_BLOCKED:
            {
                /* IPC_BLOCKED,
                 * block the task until there's a new message available */

            	/* push the caller task to the queue's stack */
            	STACK_PUSH(&queue->tcbs, sched.run);

                /* enable IRQ, ready for context switch */
                CyanTCB *tcb = sched.run;

            	cyanEnableIRQ();

                /* block the function call */
            	cyanSchTaskSuspend(&sched, tcb);

                /* at this point, the function call should be blocked and
                 * will never be resumed until msgQueueSend() is called
                 * by other task / ISR. */

                /* task resumed, there shall be a new message in the queue */
                spinLock(&queue->lock);
                {
                    if(!STAILQ_FIRST(&queue->queue))
                    {
                        panic(
                            "task resumed but there is no message in message queue\r\n"
                            "could be caused by calling this function while IRQ disabled"
                        );
                    }
                }
                spinUnlock(&queue->lock);

                /* receive the message using non-block call */
                return msgQueueFetchOnce(queue);
            }
                
            case IPC_NOWAIT:
            {
                /* no more message available */

                cyanEnableIRQ();

                return NULL;
            }
                
            default:
            {
                /* flag not found */
                panic("IPC flag not recognized");
            }
        }
    }
    else
    {
        /* message available, fetch message and return */
        void *msg = msgQueueFetchOnce(queue);

        cyanEnableIRQ();

        return msg;
    }
}
