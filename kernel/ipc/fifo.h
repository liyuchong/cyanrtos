/*
 * Copyright (c) 2013, 2015-2016 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef FIFO_H__
#define FIFO_H__

#include <stddef.h>
#include <stdlib.h>

#include <kernel/mt/memory.h>

#include <kernel/arch/blackfin/intdef.h>

#include <kernel/sys/queue.h>
#include <kernel/sys/stack.h>
#include <kernel/sys/vector.h>
#include <kernel/mt/lock.h>

/* receive flag, if there is no message available, return immediately */
#define IPC_NOWAIT   0x02

/* receive flag, block the call until there are new messages available */
#define IPC_BLOCKED  0x04

/* malloc() */
#ifndef FIFO_MALLOC
#   define FIFO_MALLOC(_size)     malloc_mt(_size)
#endif /* FIFO_MALLOC */

/* free() */
#ifndef FIFO_MFREE
#   define FIFO_MFREE(_ptr)       free_mt(_ptr)
#endif /* FIFO_MFREE */

/* msgQueueGet() flags */
/* try finding the message queue, do not create */
#define MSG_QUEUE_FIND          0x01

/* try finding the message queue, if not found, then create one. */
#define MSG_QUEUE_CREATE        0x02

/* msgQueueSend() flags */
/* normal insertion of the message to the message queue */
#define MSG_QUEUE_INSERT_NORMAL 0x01

/* insert the message to the head of the message queue */
#define MSG_QUEUE_INSERT_HEAD   0x02

/* max queues in the queue manager */
#define MSG_QUEUE_MAX_QUEUE     128

/**
 *  MsgQueueNode
 */
typedef struct MsgQueueNode
{
    /* message payload */
    void *message;
    
    /* single linked tail queue data structure */
    STAILQ_ENTRY(MsgQueueNode) next;
} MsgQueueNode;

/**
 *  MsgQueue
 */
typedef struct
{
    /* task control block */
    STACK(void *)tcbs;

    /* queue id */
    uint8_t id;
    
    uint16_t len;

    /* queue head */
    STAILQ_HEAD(msg_queue, MsgQueueNode) queue;

    /* spin lock for queue */
    SpinLock lock;
} MsgQueue;

/**
 *  Queue manager
 */
typedef struct
{
	/* queue array */
    MsgQueue *queues[MSG_QUEUE_MAX_QUEUE];

    /* spin lock for queue array */
    SpinLock lock;
} MsgQueueManager;

/**
 *  Get the size of memory occupied by the queue manager
 *
 *  @param _manager Queue manager
 *
 *  @return Size in bytes
 */
#define QUEUE_MANAGER_SIZE(_manager) (sizeof((_manager)->queues))

/**
 *  Initialize a queue manager
 *
 *  @param manager Queue manager
 *
 *  @return Input pointer to the queue manager
 */
MsgQueueManager *msgQueueManagerInit(MsgQueueManager *manager);

/**
 *  Destroy a queue manager,
 *  this function will free all memory allocated using msgQueueGet() function.
 *
 *  @param manager Queue manager
 */
void msgQueueManagerDestroy(MsgQueueManager *manager);

/**
 *  Get a message queue, create on or return NULL based on the flag setting
 *  if the specified key does not exist
 *
 *  @param manager Queue manager
 *  @param key     Message queue key
 *  @param flag    Options
 *
 *  @return A pointer to the existing message queue found or, if not found,
 *          when
 *              flag = MSG_QUEUE_FIND:   NULL
 *              flag = MSG_QUEUE_CREATE: pointer to the new message queue
 */
MsgQueue *msgQueueGet(
    MsgQueueManager *manager,
    const uint8_t key, const uint16_t flag
);

/**
 *  Enqueue a message to a given message queue
 *
 *  @param queue The message queue
 *  @param msg   The message to be enqueued
 *  @param flag  Options
 *
 *  @return 0 on success, -1 on failure
 */
int msgQueueSend(MsgQueue *queue, void *msg, const int flag);

/**
 *  Read a message and dequeue it from the given message queue
 *  NOTE:
 *      Make sure that the IRQ is enabled while calling this function, 
 *      otherwise the function will fail to block the call.
 *
 *  @param queue   The message queue
 *  @param flag    Options
 *
 *  @return payload, NULL if no more message
 */
void *msgQueueRecv(MsgQueue *queue, const int flag);

/*
 * CircularBuffer(FIFO) - a circular buffer implementation
 *
 * This implementation allows blocking and non-blocking read.
 */
typedef struct CircularBuffer
{
    size_t size;
    size_t in, out;

    uint8_t *buf;

    /* buffer lock */
    SpinLock lock;

    /* task control block */
    VECTOR(void *) tcbs;

} CircularBuffer;

/**
 *  Initialize a circular buffer, allocate memory for its buffer
 *
 *  @param size Size of the buffer, shall be power of 2
 *
 *  @return 0 on success, non-zero otherwise
 */
int circularBufferInit(CircularBuffer *fifo, size_t size);

/**
 *  Read a circular buffer
 *
 *  @param fifo Pointer to the circular buffer
 *  @param buf  Buffer
 *  @param len  Bytes to read
 *  @param flag Read flag
 *
 *  @return Bytes read
 */
size_t circularBufferRead(CircularBuffer *fifo, uint8_t *buf, size_t len, int flag);

/**
 *  Write a circular buffer
 *
 *  @param fifo Pointer to the circular buffer
 *  @param buf  Buffer
 *  @param len  Bytes to write
 */
size_t circularBufferWrite(CircularBuffer *fifo, uint8_t *buf, size_t len);

/**
 *  Test if a circular buffer is empty
 *
 *  @param fifo Pointer to the circular buffer
 *
 *  @return true if is empty, false otherwise
 */
static inline bool circularBufferEmpty(CircularBuffer *fifo)
{
	return fifo->in == fifo->out;
}

/**
 *  Test if a circular buffer is full
 *
 *  @param fifo Pointer to the circular buffer
 *
 *  @return true if is full, false otherwise
 */
static inline bool circularBufferFull(CircularBuffer *fifo)
{
	return (fifo->in - fifo->out) == fifo->size;
}

/**
 *  Reset a circular buffer
 *
 *  @param fifo Pointer to the circular buffer
 */
static inline void circularBufferReset(CircularBuffer *fifo)
{
	fifo->in = fifo->out = 0;
}

#endif /* FIFO_H__ */
