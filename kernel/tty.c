/*
 * Copyright (c) 2013, 2015-2016 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <kernel/sys/tty.h>
#include <kernel/mt/memory.h>

TTY *ttyRoot;

VECTOR(TTY *) ttys;

void ttysInit(void)
{
    SV_INIT(&ttys, 2, TTY *);

    /* create a root tty */
    ttyRoot = ttysCreate("ttys000", 4096);
    kassert(ttyRoot);
}

TTY *ttysCreate(const char *name, size_t size)
{
    TTY *tty = malloc_mt(sizeof(TTY));

    if(!tty)
    {
        goto __tty_create_oom;
    }

    /* allocate memory for in buffer and out buffer */
    tty->in =  malloc_mt(sizeof(CircularBuffer));
    tty->out = malloc_mt(sizeof(CircularBuffer));

    if(!tty->in || !tty->out)
    {
        goto __tty_create_oom;
    }

    circularBufferInit(tty->in, size);
    circularBufferInit(tty->out, size);

    /* copy tty's name */
    strncpy(tty->name, name, TTY_NAME_MAX_LEN);

    tty->loginTask = NULL;

    /* add the TTY to tty list */
    SV_PUSH_BACK(&ttys, tty);

    return tty;

__tty_create_oom:
    printk("ttyCreate: cannot create tty: out of memory\r\n");

    if(tty)
    {
        if(tty->in)
        {
            free_mt(tty->in);
            tty->in = NULL;
        }

        if(tty->out)
        {
            free_mt(tty->out);
            tty->out = NULL;
        }

        free_mt(tty);
        tty = NULL;
    }

    return NULL;
}

TTY *ttysGet(const char *name)
{
    for(int i = 0; i < SV_SIZE(&ttys); i++)
    {
        if(strcmp(SV_AT(&ttys, i)->name, name) == 0)
        {
            return SV_AT(&ttys, i);
        }
    }

    return NULL;
}
