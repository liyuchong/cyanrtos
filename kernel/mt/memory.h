/*
 * Copyright (c) 2013, 2015-2016 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

/* Multi-threading safe memory allocation & deallocation */

#ifndef MEMORY_H__
#define MEMORY_H__

#include <stdlib.h>

#include <kernel/mt/lock.h>
#include <lib/tlsf/tlsf.h>

#define MEM_MAN_INFO "TLSF Memory Allocator, build " __DATE__ ", " __TIME__

#define TLSF_POOL_SIZE (8 * 1024 * 1024)

/**
 *  initialize a spin lock for memory allocation & deallocation
 */
void cyanMemInit(void);

/**
 *  Allocate memory
 *  
 *  @param size Size
 *
 *  @return Pointer to the allocated memory address,
 *  NULL if failed.
 */
void *malloc_mt(size_t size);

/**
 *  Free an allocated piece of memory
 *  
 *  @param ptr Pointer to the allocated memory
 */
void free_mt(void *ptr);

/**
 *  Memory reallocation
 *  
 *  @param ptr  Pointer to the original memory address
 *  @param size Size
 *
 *  @return Pointer to the allocated memory address,
 *  NULL if failed.
 */
void *realloc_mt(void *ptr, size_t size);

/**
 *  Check the integrity of the allocator, returns non-zero if failed
 */
int memHeapCheck(void);

/* memory allocation/deallocation reference counter.
 * incremented on malloc_mt, decremented on free_mt. */
extern atomic_t memOperationCounter;

/* free memory counter */
extern atomic_t memFreeCounter;

#endif /* MEMORY_H__ */
