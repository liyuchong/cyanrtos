/*
 * Copyright (c) 2013, 2015-2016 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <lib/tlsf/tlsf.h>
#include <kernel/mt/memory.h>
#include <kernel/arch/blackfin/intdef.h>
#include <kernel/sys/utils.h>

/* TLSF heap pool */
tlsf_pool memPool;

/* memory allocator spin lock */
SpinLock memLock = SPIN_LOCK_UNLOCKED();

/* operation reference counter */
atomic_t memOperationCounter = ATOMIC_INIT(0);

/* free memory counter */
atomic_t memFreeCounter = ATOMIC_INIT(TLSF_POOL_SIZE);

void cyanMemInit(void)
{
    /* get pool from system heap */
    uint8_t *pool = malloc(TLSF_POOL_SIZE);

    if(!pool)
    {
        panic("memory allocation failed");
    }

    printk("memory: system heap\r\n");
    printk("\ttotal %d bytes\r\n", TLSF_POOL_SIZE);
    printk("\tstart address 0x%p\r\n", pool);
    printk("\tend address   0x%p\r\n", pool + TLSF_POOL_SIZE);

    /* create a pool for TLSF allocator */
    memPool = tlsf_create(pool, TLSF_POOL_SIZE);

    printk("memory: TLSF pool created\r\n");
}

void *malloc_mt(size_t size)
{
    void *ret = NULL;

    spinLock(&memLock);
    {
        atomicInc(&memOperationCounter);

        ret = tlsf_malloc(memPool, size);

        /* subtract the size of the block from the memory counter */
        atomicSub(tlsf_block_size(ret), &memFreeCounter);
    }
    spinUnlock(&memLock);

    return ret;
}

/**
 *  Free an allocated piece of memory
 *
 *  @param ptr Pointer to the allocated memory
 */
void free_mt(void *ptr)
{
    spinLock(&memLock);
    {
        /* Do not decrement OPC if the ptr is NULL:
         *
         * ISO/IEC 9899:1999
         * 7.20.3.2
         * If ptr is a null pointer, no action occurs.
         */
        if(ptr)
        {
            atomicDec(&memOperationCounter);
        }

        /* add the size of the block to free memory counter */
        atomicAdd(tlsf_block_size(ptr), &memFreeCounter);

        tlsf_free(memPool, ptr);
    }
    spinUnlock(&memLock);
}

/**
 *  Memory reallocation
 *
 *  @param ptr  Pointer to the original memory address
 *  @param size Size
 *
 *  @return Pointer to the allocated memory address,
 *  NULL if failed.
 */
void *realloc_mt(void *ptr, size_t size)
{
    void *ret = NULL;

    spinLock(&memLock);
    {
        /* Increment OPC by 1 if ptr is NULL
         *
         * ISO/IEC 9899:1999
         * 7.20.3.4
         * If ptr is a null pointer, the realloc function behaves like
         * the malloc function for the specified size.*/
        if(!ptr)
        {
            atomicInc(&memOperationCounter);
        }

        /* size of the original block */
        const size_t originalSize = tlsf_block_size(ptr);

        /* first assume that realloc will success, so add the size of the original
         * block to free memory size counter */
        atomicAdd(originalSize, &memFreeCounter);

        /* do realloc */
        ret = tlsf_realloc(memPool, ptr, size);

        if(ret)
        {
            /* realloc success, subtract the size of the new block from
             * free memory counter */
            atomicSub(tlsf_block_size(ret), &memFreeCounter);
        }
        else
        {
            /* realloc failed, subtract the original block size from the
             * free memory counter, which shall remain unchanged. */
            atomicSub(originalSize, &memFreeCounter);
        }
    }
    spinUnlock(&memLock);

    return ret;
}

int memHeapCheck(void)
{
    return tlsf_check_heap(memPool);
}
