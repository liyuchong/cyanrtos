/*
 * Copyright (c) 2013, 2015-2016 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <kernel/sys/sys.h>
#include <kernel/sched/scheduler.h>
#include <kernel/sched/queue_ops.h>

Semaphore *semaphoreInit(Semaphore *sem, int value)
{
    /* reset semaphore count */
    atomicSet(&sem->value, value);

    /* initialize task wait list and spin lock */
    SV_INIT(&sem->waitList, 2, CyanTCB *);
    spinLockInit(&sem->lock);

    return sem;
}

void semaphoreWait(Semaphore *lock)
{
    atomicDec(&lock->value);

    if(atomicRead(&lock->value) < 0)
    {
        /* resource busy, get the current task and put it in the wait list */
        CyanTCB *tcb = sched.run;

        spinLock(&lock->lock); {
        	SV_PUSH_BACK(&lock->waitList, tcb);
        } spinUnlock(&lock->lock);

        printk("suspend: %s\r\n", tcb->name);
        /* block the function call */
        cyanSchTaskSuspend(&sched, tcb);
    }
}

void semaphorePost(Semaphore *lock)
{
    atomicInc(&lock->value);

    if(atomicRead(&lock->value) <= 0)
    {
        /* get the first task in the wait list and remove it from the list */
        CyanTCB *tcb = SV_AT(&lock->waitList, 0);

       spinLock(&lock->lock); {
    	   SV_ERASE_AT(&lock->waitList, 0);
       } spinUnlock(&lock->lock);

        /* resume the task in the wait list */
        cyanSchTaskResume(&sched, tcb);
    }
}
