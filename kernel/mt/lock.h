/*
 * Copyright (c) 2013, 2015-2016 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SPIN_LOCK_H__
#define SPIN_LOCK_H__

#include <kernel/arch/blackfin/atomic.h>


/**
 *  Spin lock: ticket lock
 *  A FIFO spin lock implementation
 *  Spin locks in general must always protect against preemption, as it is
 *  an error to perform any type of context switch while holding a spin lock.
 */
typedef struct
{
    atomic_t ticket;
    atomic_t users;

    /* variable for saving local IRQ flags */
    int irqFlags;
} SpinLock;

#define SPIN_LOCK_UNLOCKED() { ATOMIC_INIT(0), ATOMIC_INIT(0) }

/**
 *  Initialize a given spin lock
 *
 *  @param lock A pointer to a spin lock
 */
void spinLockInit(SpinLock *lock);

/**
 *  Test if a spin lock is lockable
 *
 *  @param lock A pointer to a spin lock
 *
 *  @return 1 if lockable, 0 otherwise
 */
static inline int spinLockable(SpinLock *lock)
{
    /* make a copy of the lock */
    SpinLock u = *lock;

    return atomicCompare(&u.ticket, &u.users);
}

/**
 *  Lock a spin lock
 *
 *  @param lock A pointer to a spin lock
 *
 */
void spinLock(SpinLock *lock);

/**
 *  Unlock a spin lock
 *
 *  @param lock A pointer to a spin lock
 */
static inline void spinUnlock(SpinLock *lock)
{
    int flags;

    /* enter critical section */
    local_irq_save(flags);
    {
        /* set `now serving' to next user */
        lock->ticket.v++;

        /* reset ticket and users if lockable to prevent overflow */
        if(spinLockable(lock))
        {
            spinLockInit(lock);
        }
    }
    local_irq_restore(flags);

    /* restore local IRQ flags */
    local_irq_restore(lock->irqFlags);
}

#endif /* SPIN_LOCK_H__ */
