/*
 * Copyright (c) 2013, 2015-2016 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <kernel/sys/kern_lib.h>
#include <cdefBF533.h>

char __getchar(void)
{
	uint8_t ch;

	size_t n = circularBufferRead(sched.run->tty->in, &ch, 1, IPC_BLOCKED);
	kassert(n == 1);

	return ch;
}

CyanTCB *cyanFindTask(uint32_t pid)
{
    CyanTCB *tcb;

    spinLock(&sched.ready.lock);
    STAILQ_FOREACH(tcb, &sched.ready.q, next)
    {
        if(tcb->pid == pid)
        {
            spinUnlock(&sched.ready.lock);
            goto __tcb_found;
        }

    }
    spinUnlock(&sched.ready.lock);

    /* print sleeping tasks */
    spinLock(&sched.sleep.lock);
    STAILQ_FOREACH(tcb, &sched.sleep.q, next)
    {
        if(tcb->pid == pid)
        {
            spinUnlock(&sched.sleep.lock);
            goto __tcb_found;
        }
    }
    spinUnlock(&sched.sleep.lock);

    /* print stuck tasks */
    spinLock(&sched.stuck.lock);
    STAILQ_FOREACH(tcb, &sched.stuck.q, next)
    {
        if(tcb->pid == pid)
        {
            spinUnlock(&sched.stuck.lock);
            goto __tcb_found;
        }
    }
    spinUnlock(&sched.stuck.lock);

    return NULL;

__tcb_found:
    return tcb;
}

int isnum(const char *str)
{
    char *p = (char *)str;
    while(*p)
    {
        if(!isdigit(*p++))
            return 0;
    }

    return 1;
}

void schPrintQueue(CyanScheduler *sch)
{
	uprintf("\r\n");
	uprintf(__TERM_FG_BLUE);
	uprintf("Scheduler dump\r\n");
	uprintf(__TERM_RESET);
	uprintf("ready: 0x%x sleep: 0x%x stuck: 0x%x\r\n",
			&sch->ready.q,
			&sch->sleep.q,
			&sch->stuck.q);

    uprintf("\r\nRUN:   %s[%d]\r\n",
    		sch->run ? sch->run->name : "(NULL)",
    		sch->run ? sch->run->pid : 0);

    uprintf("READY: ");
    CyanTCB *tcb;
    STAILQ_FOREACH(tcb, &sch->ready.q, next)
    {
    	uprintf("%s[%d], ", tcb->name, tcb->pid);
    }
    uprintf("\r\n");

    uprintf("SLEEP: ");
    STAILQ_FOREACH(tcb, &sch->sleep.q, next)
    {
    	uprintf("%s[%d], ", tcb->name, tcb->pid);
    }
    uprintf("\r\n");

    uprintf("STUCK: ");
    STAILQ_FOREACH(tcb, &sch->stuck.q, next)
    {
    	uprintf("%s[%d], ", tcb->name, tcb->pid);
    }
    uprintf("\r\n");

    uprintf(__TERM_FG_BLUE);
    uprintf("\r\nRegisters dump\r\n");
    uprintf(__TERM_RESET);
    uprintf("IMASK: 0x%08x  IPEND: 0x%08x  ILAT: 0x%08x\r\n",
    		*pIMASK, *pIPEND, *pILAT);
}

void __printSchedQueues(void)
{
	schPrintQueue(&sched);
}

void cyanTcbPrint(CyanScheduler *sch, CyanTCB *tcb, uint32_t flag)
{
    qprintf(" %5d ", tcb->pid);

    if(tcb->parent)
    {
    	qprintf(" %5d", tcb->parent->pid);
    }
    else
    {
    	qprintf(" %5d", tcb->pid);
    }

    uint32_t seconds = tcb->stat.cpuTime / CYAN_RTOS_TICKS_PER_SEC;
    uint16_t h = seconds / 3600;
    uint8_t m = (seconds - h * 3600) / 60;
    uint8_t s = (seconds - h * 3600) % 60;

    qprintf( "%4d:%02d.%02d", h, m, s);

    qprintf(" %4d", tcb->stat.cpuUsage / 10);
    qprintf(".%1d", tcb->stat.cpuUsage % 10);

    qprintf(" %5d", tcb->priority);

    if(flag & P_RR)
    {
        qprintf("  %6d ", tcb->round);

        qprintf(" %4d", tcb->roundCount);
    }

    if(tcb->stat.contextSwitch < 9999999)
    {
    	qprintf(" %9d ", tcb->stat.contextSwitch);
    }
    else
    {
    	qprintf(" %8dK ", tcb->stat.contextSwitch / 1000);
    }


    if(flag & P_STK)
    {
        qprintf(" %4d/%-5d ",
        		tcb->stackFree == -1 ? 0 :
        		(tcb->stackSize - tcb->stackFree) * sizeof(stack_t),
        		tcb->stackSize * sizeof(stack_t));

    }

    if(tcb == sch->run)
        qprintf("RUNNING    ");
    else if(tcb->queue == &sch->ready)
        qprintf("READY_WAIT ");
    else if(tcb->queue == &sch->sleep)
        qprintf("SLEEPING   ");
    else if(tcb->queue == &sch->stuck)
        qprintf("STUCK      ");
    else
        qprintf("UNKNOWN    ");

    /* print TTY */
    if(tcb->tty)
    {
    	qprintf("%-8s ", tcb->tty->name);
    }
    else
    {
    	qprintf("%-8s ", "??");
    }


    if(flag & P_WAIT)
    {
    	if(tcb->waitingFor)
    	{
    		qprintf( "%-6d", tcb->waitingFor->pid);
    	}
    	else
    	{
    		qprintf( "%-6s", "??");
    	}

    }

    if(!tcb->args)
    {
        qprintf("%-16s ", tcb->name);
    }
    else
    {
        for(int i = 0; i < SV_SIZE(tcb->args); i++)
        {
            qprintf("%s ", SV_AT(tcb->args, i));
        }
    }

    qprintf("\r\n");

    return;
}

static uint8_t __vectorFromQueue(CyanTcbQueue *queue, VECTOR(CyanTCB *) *vector)
{
    uint8_t counter = 0;
    CyanTCB *tcb;

    spinLock(&queue->lock);
    {
        tcb = STAILQ_FIRST(&queue->q);
        STAILQ_FOREACH(tcb, &queue->q, next)
        {
            counter++;
            SV_PUSH_BACK(vector, tcb);
        }
    }
    spinUnlock(&queue->lock);

    return counter;
}

void cyanSchPrintStatus(CyanScheduler *const sch, uint32_t flag)
{
    /* create a vector for TCBs */
    VECTOR(CyanTCB *) tcbs;
    SV_INIT(&tcbs, 2, CyanTCB *);

    /* calling qprintf() inside the iteration is not allowed - this will
     * mess up the task queues because qprintf() uses message queue. so we
     * need to push all tasks into a vector and print them. */

    /* push TCBs to the vector */
    uint8_t readyTaskCtr = __vectorFromQueue(&sch->ready, (void *)&tcbs);
    uint8_t sleepTaskCtr = __vectorFromQueue(&sch->sleep, (void *)&tcbs);
    uint8_t stuckTask    = __vectorFromQueue(&sch->stuck, (void *)&tcbs);

    qprintf("Processes: %d total, %d ready, %d sleeping, %d stuck\r\n",
    		readyTaskCtr + sleepTaskCtr + stuckTask,
    		readyTaskCtr, sleepTaskCtr, stuckTask);

    qprintf("\r\n");
    qprintf("%s", "   PID   PPID      TIME   %CPU  PRIO");
    if(flag & P_RR)
    {
    	qprintf("   ROUND  CNT.");
    }
    qprintf("   CTX.SW.");

    if(flag & P_STK)
    {
    	qprintf("  FREE/STACK");
    }

    qprintf(" STATE    ");

    qprintf("  TTY    ");

    if(flag & P_WAIT)
    {
    	qprintf("  WAIT");
    }

    qprintf("  COMMAND");

    qprintf("\r\n");

    /* sort */
    // XXX: move to vector.h
    int i, j;
    for(i = 0; i < SV_SIZE(&tcbs); i++)
    {
        for(j = 0; i + j < SV_SIZE(&tcbs) - 1; j++)
        {
            if(SV_AT(&tcbs, j)->pid > SV_AT(&tcbs, j + 1)->pid)
            {
                CyanTCB *temp = SV_AT(&tcbs, j);
                tcbs.data[j] = tcbs.data[j + 1];
                tcbs.data[j + 1] = temp;
            }
        }
    }

    /* print TCBs */
    for(int i = 0; i < SV_SIZE(&tcbs); i++)
    {
        cyanTcbPrint(sch, SV_AT(&tcbs, i), flag);
    }

    SV_RELEASE(&tcbs);
}

#define KEY_BS '\b'
#define KEY_DEL 127
#define KEY_ESC 27
#define KEY_TAB '\t'

#define ESC_ARROW_LEFT  10
#define ESC_ARROW_RIGHT 11
#define ESC_ARROW_UP    12
#define ESC_ARROW_DOWN  13

char *readline(const char *prompt, char *buffer)
{
	char line[512];
		memset(line, 0, 512);
		int count = 0;

		if(strlen(prompt) > 0)
			qprintf((char *)prompt);

	    bool stateEscape = false;
	    int escapeCharCount = 0;
	    char escapeChars[8];

	    while (1)
	    {
			char c = __getchar();

			switch (c)
			{
				/* back space / delete */
				case KEY_BS:
				case KEY_DEL:
				{
					if(count != 0)
					{
						line[--count] = '\0';
						qprintf("\b \b");
					}

					break;
				}

				case '\n': break; /* ignore */

				/* escape, let's ignore this for now */
				case KEY_ESC:
				{
					stateEscape = true;

					break;
				}

				case KEY_TAB:
				{
					for(int i = 0; i < 4; i++)
					{
						uputchar(' ');
						line[count++] = ' ';
					}

					break;
				}

				/* other characters */
				default:
				{
					//qprintf("%c", c);
					uputchar(c);

					if(c == '\r')
					{
						count = 0;
						qprintf("\r\n");

						strcpy(buffer, line);

						return buffer;
					}
					else
					{
						line[count++] = c;
						line[count] = '\0';
					}
				}
			}

	    }
}

char *readmore(char *prompt, char *buf)
{
    char line[512];
    memset(line, 0, 512);

    readline(prompt, line);

    size_t len = strlen(line);

    /* valid input */
    if(len > 0)
    {
        /* continue line */
        if(line[len - 1] == '\\')
        {
            line[strlen(line) - 1] = '\0';

            strcat(line, "\r\n");
            strcat(buf, line);

            int l = 1;

            while(1)
            {
            	char mp[16];
            	sprintf(mp, "\033[36m" "%3d> " __TERM_RESET, ++l);

                readline(mp, line);

                if(strlen(line) > 0)
                {
                    strcat(line, "\r\n");
                    strcat(buf, line);
                }
                else
                {
                    return buf;
                }
            }
        }
        else
        {
            strcpy(buf, line);

            return buf;
        }
    }

    return buf;
}
