/*
 * Copyright (c) 2013, 2015-2016 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef STACK_H__
#define STACK_H__

#include <kernel/sys/utils.h>

/**
 *  Declare a stack
 *
 *  @param _type Type of the element in the stack
 */
#define STACK(_type)    \
struct                  \
{                       \
    _type *s;           \
    size_t top;         \
    size_t depth;       \
}

/**
 *  Initialize a stack
 *
 *  @param _stack Pointer to a stack
 *  @param _type  Type of the element
 *  @param _depth Depth of the stack
 */
#define STACK_INIT(_stack, _type, _depth)               \
do {                                                    \
    (_stack)->top = 0;                                  \
    (_stack)->depth = _depth;                           \
    (_stack)->s = malloc_mt(sizeof(_type) * (_depth)) ; \
} while(0)

/**
 *  Stack PUSH operation
 *
 *  @param _stack Pointer to a stack
 *  @param _data  Data to be pushed
 */
#define STACK_PUSH(_stack, _data)                       \
do {                                                    \
    kassert((_stack)->top + 1 <= (_stack)->depth);      \
    (_stack)->s[(_stack)->top++] = _data;               \
} while(0)

/**
 *  Stack POP operation
 *
 *  @param _stack Pointer to a stack
 *
 *  @return Top element of the stack
 */
#define STACK_POP(_stack)   \
    (kassert((_stack)->top), (_stack)->s[--(_stack)->top])

/**
 *  Get the top element of the stack without popping it
 *
 *  @param _stack Pointer to a stack
 *
 *  @return Top element of the stack
 */
#define STACK_TOP(_stack)   \
    (_stack)->s[(_stack)->top - 1]

/**
 *  Erase an element in a stack with a given value
 *  Note that this function only erases the first occurrence
 *
 *  @param _stack Pointer to the stack
 *  @param _type  Type of the element
 *  @param _val   Value to be erased
 */
#define STACK_ERASE_VALUE(_stack, _type, _val)                  \
do {                                                            \
    size_t i;                                                   \
    for(i = 0; i < STACK_COUNT(_stack); i++)                    \
    {                                                           \
        if((_stack)->s[i] == (_val))                            \
        {                                                       \
            memcpy((_stack)->s + (i),                           \
                    (_stack)->s + i + 1,                        \
                    sizeof(_type) * ((_stack)->top - i - 1));   \
            (_stack)->top--;                                    \
            break;                                              \
        }                                                       \
    }                                                           \
} while(0)

/**
 *  Get number of elements in the stack
 *
 *  @param _stack Pointer to a stack
 *
 *  @return Number of elements
 */
#define STACK_COUNT(_stack) \
    (_stack)->top

/**
 *  Release memory allocated for the stack
 *
 *  @param _stack Pointer to the stack
 */
#define STACK_RELEASE(_stack) \
    free_mt((_stack)->s)

#endif /* STACK_H__ */
