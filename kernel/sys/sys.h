/*
 * Copyright (c) 2013, 2015-2016 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SYS_H__
#define SYS_H__

#include <stdio.h>

#include <kernel/arch/blackfin/cpu.h>
#include <kernel/sys/task.h>
#include <kernel/mt/lock.h>
#include <kernel/sys/ipc.h>
#include <kernel/ipc/fifo.h>
#include <kernel/sys/utils.h>
#include <lib/tree/avl.h>
#include <kernel/sys/tty.h>

#include <lib/rtos_print.h>

typedef struct SchedStat
{
    uint32_t tick;
    uint8_t readyTaskCount;
} SchedStat;

/**
 *  CyanScheduler
 */
typedef struct CyanScheduler
{
    /* scheduler tick period in Hz */
    uint16_t tickFrequency;

    /* scheduler tick count */
    uint32_t tick;
    uint16_t priodTick;

    void *task;

    /*
     * running task, ready tasks and sleeping tasks.
     *
     * run:   task being executed on the CPU currently.
     * ready: tasks which are ready to be run on the CPU, currently waiting
     *        for scheduling.
     * sleep: tasks which are sleeping, the scheduler won't schedule them.
     */

    /* current running task */
    CyanTCB *run;

    /* number of tasks which have the highest priority */
    uint8_t readyTaskCount;

    /* ready queue */
    CyanTcbQueue ready;

    /* sleep queue */
    CyanTcbQueue sleep;

    /* stuck queue */
    CyanTcbQueue stuck /* in reverse */;

} CyanScheduler;

typedef struct
{
    avl_node_t link;

    char *key;
    char *value;
} EnvEntry;

#define KM_KILL 9
#define KM_DAEMON 10

typedef struct KernelMessage
{
    int msgid;
    void *data;
} KernelMessage;

/* interrupt nesting level            -> CyanCore.c */
extern  uint8_t intNesting;
/* indicates if the kernel is running -> CyanCore.c */
extern  uint32_t running;
/* pointer to currently running task  -> CyanCore.c */
extern  CyanTCB *cyanTcbCurrent;
/* pointer to the ready task          -> CyanCore.c */
extern  CyanTCB *cyanTcbReady;

extern uint32_t pidMax;

/* scheduler */
extern CyanScheduler sched;

/**
 *  System tick, should only be call by the core timer timeout interrupt
 */
void cyanSystemTick(void);

/**
 *  Start the task which cyanTcbReady is pointing to
 */
void cyanStartReadyTask(void);

/**
 *  Register some essential event handlers
 *      IVG14 -> context switch trap
 *      IVG6  -> core timer timeout -> scheduler tick
 *
 *      then enable IVG14.
 */
void cyanInitEventVector(void);

/**
 *  Release memory of an argument list
 *
 *  @param args Pointer to an argument list
 */
void cyanReleaseArgumentList(Arguments *args);

/* env.c
 * Environment variable subsystem
 * Implemented using an AVL tree */

/**
 *  Initialize the environment variable tree
 */
int __envinit(void);

/**
 *  Set the value of an environment variable
 *
 *  @param name      Name of the environment variable
 *  @param value     Value of the environment variable
 *  @param overwrite Indicate if the existing environment variable should be
 *                   overwritten
 *
 *  @return 0 if succeeded, non-zero otherwise
 */
int __setenv(const char *name, const char *value, int overwrite);

/**
 *  Get the value of an environment variable
 *
 *  @param name Name of the environment variable
 *
 *  @return Value of the environment variable
 */
char *__getenv(const char *name);

/* built-in tasks */

/**
 *  Idle task daemon
 */
void idled(int argc, const char *argv[]);

/**
 *  Statistic task dameon
 */
void statd(int argc, const char *argv[]);

/**
 *  IO daemon
 */
void iod(int argc, const char *argv[]);

void envctl(int argc, const char *argv[]);

void mem(int argc, const char *argv[]);

void kill(int argc, const char *argv[]);

void ps(int argc, const char *argv[]);

void top(int argc, const char *argv[]);

void queues(int argc, const char *argv[]);

void uname(int argc, const char *argv[]);

void kernelMessageLoop(void);

#endif /* SYS_H__ */
