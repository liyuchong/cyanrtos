/*
 * Copyright (c) 2013, 2015-2016 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef UTILS_H_
#define UTILS_H_

#include <lib/rtos_print.h>

/**
 *  kernel level print implementation
 */
#define printk(format, ...) 							\
	do { 												\
		uprintf(format, ##__VA_ARGS__);					\
	} while(0)

extern void __printSchedQueues(void);

#define __TERM_FG_GREEN  "\033[32m"
#define __TERM_FG_BLUE   "\033[34m"
#define __TERM_HIGHLIGHT "\033[31m"
#define __TERM_RESET 	 "\033[0m"

/**
 *  kernel panic, display a message and self spin
 *
 *  @param _msg Message
 */
#define panic(_msg)                                     		\
do {                                                    		\
	printk("\r\n");                                             \
	printk(__TERM_HIGHLIGHT); 						    		\
    printk("KERNEL PANIC: %s", _msg );							\
    printk(__TERM_RESET); 										\
    printk("\r\nline: %d, file: %s.\r\n", 						\
           __LINE__, __FILE__);                 				\
    __printSchedQueues();       								\
    printk("\r\nWe're hanging here...\r\n");				    \
    /* self spin */                                     		\
    while(1);                                           		\
} while(0)

/**
 *  kernel level assertion test
 *  if the assertion fails, a kernel panic will be issued
 *
 *  @param e Assertion condition
 */
void __assertionFailed(const char *msg);

#define kassert(_test)	\
	((_test) ? 			\
		(void)0 : 		\
		__assertionFailed(__FILE__ ":" _STRIZE(__LINE__) " (" #_test ")"))

#define kassert2(_test) if(!(_test)) panic("kernel level assertion failed:");

#endif /* UTILS_H_ */
