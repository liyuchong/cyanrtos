/*
 * Copyright (c) 2013, 2015-2016 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef CYAN_RTOS_H__
#define CYAN_RTOS_H__

#include <stdbool.h>

#include <kernel/sys/sys.h>

/* RTOS basic configurations */
/* Operating system scheduler tick count per second,
 * this is the frequency of the scheduler tick, which is triggered using
 * the processor's core timer. */
#define CYAN_RTOS_TICKS_PER_SEC        100

/* Default stack size for a task */
#define DEFAULT_STACK_SIZE   512

#define QUEUE_STDIN    0x01

#define QUEUE_STDOUT   0x02

/* RTOS major version number
 * This is incremented when the API is changed in non-compatible ways */
#define CYAN_RTOS_VER_MAJOR "2"

/* RTOS minor version number
 * This is incremented when features are added to the API but it remains
 * backward-compatible.*/
#define CYAN_RTOS_VER_MINOR "0"

/* RTOS revision version number
 * This is incremented when a bug fix release is made that does not
 * contain any API changes */
#define CYAN_RTOS_VER_REV   "1"

/* Version string, split using dot */
#define CYAN_RTOS_VERSION                               \
    CYAN_RTOS_VER_MAJOR "."                             \
    CYAN_RTOS_VER_MINOR "."                             \
    CYAN_RTOS_VER_REV

/* RTOS name string, version, CPU type and date of build */
#define CYAN_RTOS_UNAME ("CyanRTOS "                    \
    CYAN_RTOS_VERSION " on " CYAN_RTOS_CPU_NAME         \
    "; build " __DATE__ ", " __TIME__)

/**
 *  Initialize the RTOS
 */
void cyanInitRTOS(void);

/**
 *  Start the scheduler
 *  This function also enables the core timer timeout interrupt
 */
void cyanSchedulerStart(void);

/**
 *  Create a task
 *
 *  @param task      Pointer to task
 *  @param args      Argument list passed to the task
 *  @param stackSize Size of task's stack
 *  @param name      Name of the task
 *  @param priority  Priority of the task
 *  @param round     Time round of the task(RR)
 */
CyanTCB *cyanTaskCreate(
    void (*task)(int, const char *[]), Arguments *args, uint32_t stackSize,
    char *name, uint8_t priority, uint16_t round, bool shcedNow
);

/*
 *  Initialize a given TCB and insert it into the scheduler
 *
 *  @param tcb      A pointer to the TCB
 *  @param task     Task function
 *  @param arg      Argument pointer
 *  @param stackTop A pointer to the top of the task's stack
 *  @param name     Name of the task
 *  @param priority Priority of the task
 *  @param round    Round of the task(for RR scheduling)
 */
CyanTCB *cyanTaskInit(
    CyanTCB *tcb,
    void (*task)(int, const char *[]), Arguments *arg,
    stack_t *stackTop, size_t stackSize,
    char *name, const uint8_t priority, const uint16_t round, bool resched
);

/* interrupt managements */
/**
 *  Register a handler of a given interrupt
 *
 *  @param ivg     IVG number of the interrupt
 *  @param handler Handler function
 */
void cyanRegisterInterruptHandler(uint8_t ivg, InterruptHandler handler, bool nesting);

/**
 *  Enable a given interrupt
 *
 *  @param ivg IVG number of the interrupt
 */
void cyanEnableInterruptEntry(uint8_t mask);

/**
 *  Disable a given interrupt
 *
 *  @param ivg IVG number of the interrupt
 */
void cyanDisableInterruptEntry(uint8_t mask);

/**
 *  Sleep current task by a given tick count
 *  NOTE: this function acts on the task which is currently running
 *  on the CPU. To sleep other tasks which are not currently running,
 *  use cyanSchTaskSleep().
 *
 *  @param _tick Tick count to sleep
 */
#define tsleep(_tick) cyanSchTaskSleep(&sched, sched.run, _tick)

/**
 *  Suspend execution for an interval of time
 *
 *  @param seconds Time to suspend
 */
void sleep(int seconds);

void msleep(int milliseconds);

/**
 *  Kill a task and clear its memory
 *  Assertions:
 *      1) tcb is not NULL
 *      2) tcb belongs to some queue
 *
 *  @param tcb Task to be killed
 */
void cyanKillTask(CyanTCB *tcb);

/**
 *  Queued printf
 */
void qprintf(char *fmt, ...);

// XXX: wtf?
int exec_cmd(char *cmd);

int exec_args(int argc, const char *argv[]);

#define wait(_tcb) cyanSchWait(&sched, _tcb)

int findCommand(char *const command);

/**
 *  Get PID of the currently running task
 */
static inline uint32_t getpid(void)
{
    kassert(sched.run);

    return sched.run->pid;
}

#define setenv(_name, _value, _overwrite) __setenv(_name, _value, _overwrite)

#define getenv(_name) __getenv(_name)

#endif /* CYAN_RTOS_H__  */
