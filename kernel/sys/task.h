/*
 * Copyright (c) 2013, 2015-2016 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef TASK_H_
#define TASK_H_

#include <kernel/arch/blackfin/cpu.h>
#include <kernel/mt/lock.h>

#include <kernel/sys/queue.h>
#include <kernel/sys/vector.h>
#include <kernel/sys/ipc.h>
#include <kernel/sys/signal.h>
#include <kernel/sys/tty.h>

#define CYAN_RTOS_TASK_NAME_LEN 16

/* Single linked tail queue of CyanTCB */
typedef struct
{
    /* task queue */
    STAILQ_HEAD(, CyanTCB) q;
    /* spin lock */
    SpinLock lock;
} CyanTcbQueue;

typedef struct
{
    /* number of scheduler ticks on this task */
    uint32_t cpuTime;

    /* number of context switch performed on this task */
    uint32_t contextSwitch;

    /* CPU time counter for statd task */
    uint16_t periodTime;

    /* CPU usage, fixed point */
    uint16_t cpuUsage;
} CyanTaskStat;

/**
 * Argument list is a vector of char pointer
 */
typedef VECTOR(char *) Arguments;

/**
 *  CyanTCB
 *  Task Control Block
 */
typedef struct CyanTCB
{
    /* pointer to the task's current stack top */
    /* this shall be the first field of TCB */
    stack_t *stack;
    stack_t *stackPtr;

    /* task's parent task */
    struct CyanTCB *parent;

    /* pointer to the next task */
    /* SINGLY-LINKED TAIL QUEUE from FreeBSD kernel */
    STAILQ_ENTRY(CyanTCB) next;

    /* task identifier */
    char *name;

    /* task PID */
    uint32_t pid;

    /* pointer to task */
    void (*main)(int, const char *[]);

    /* call back function which will be called before task being killed.
     * return any non-zero to abort the kill routine */
    int (*exiting)(int, const char *[]);

    /* task argument list */
    Arguments *args;

    /* current TTY of the task, qprintf() and
     * getchar(), readline() & readmore() etc. are interfaced with
     * this specific TTY by default */
    TTY *tty;

    /* file descriptor of the specific task
     * the descriptor can be modified to achieve in/out redirection */
    VECTOR(CircularBuffer *) fds;

    /* task queue which the task currently belongs to */
    CyanTcbQueue *queue;

    /* signals linked list & lock */
    STAILQ_HEAD(, SignalNode) signalHandlers;
    SpinLock signalHandlersLock;

    STAILQ_HEAD(, SignalNode) signals;
    SpinLock signalsLock;

    /* wait list, these tasks will be resumed upon task return */
    VECTOR(struct CyanTCB *) waitList;

    /* the task which this task is waiting for */
    struct CyanTCB *waitingFor;

    size_t stackSize;
    int32_t stackFree;

    /* suspend counter, task shall be resumed only when the counter is 0 */
    atomic_t suspendCounter;

    /* task statistic info: CPU usage, CPU time... */
    CyanTaskStat stat;

    /* round and count */
    uint16_t round, roundCount;

    /* sleep system tick counter */
    uint16_t sleep;

    /* task priority,
     * the lower the value the higher the priority */
    uint8_t priority;

} CyanTCB;

/**
 *  Initializes a given task's stack
 *
 *  @param task     Task
 *  @param args     Argument list
 *  @param stackTop Top of the stack
 *
 *  @return pointer to the initialized stack
 */
stack_t *cyanTaskStackInit(
    void (*task)(int, const char *[]),
    Arguments *args,
    stack_t *stackTop
);

static inline void taskWaitReferenceInc(CyanTCB *task)
{
    atomicInc(&task->suspendCounter);

#ifdef TASK_WAIT_REF_DEBUG
    uprintf("  after inc: task: %s, wc: %d\r\n",
            main->name, atomicRead(&main->suspendCounter));
#endif
}

static inline void taskWaitReferenceDec(CyanTCB *task)
{
    atomicDec(&task->suspendCounter);

#ifdef TASK_WAIT_REF_DEBUG
    uprintf("  after inc: task: %s, wc: %d\r\n",
            main->name, atomicRead(&main->suspendCounter));
#endif
}

/**
 *  Semaphore
 */
typedef struct
{
    /* value of the semaphore */
    atomic_t value;

    /* tasks which are waiting for the resource
     * normally there won't be too many tasks waiting for a semaphore, so
     * the cost of using a vector is small enough to be ignored. */
    VECTOR(CyanTCB *) waitList;
    SpinLock lock;

} Semaphore;

/*
 *  Initialize a semaphore
 *
 *  @param sem   Pointer to the semaphore
 *  @param value Value of the semaphore
 *
 *  @returm Pointer to the semaphore
 **/
Semaphore *semaphoreInit(Semaphore *sem, int value);

/*
 *  Semaphore P operation
 *
 *  @param sem Pointer to the semaphore
 **/
void semaphoreWait(Semaphore *lock);

/*
 *  Semaphore V operation
 *
 *  @param sem Pointer to the semaphore
 **/
void semaphorePost(Semaphore *lock);

#endif /* TASK_H_ */
