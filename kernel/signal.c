/*
 * Copyright (c) 2013, 2015-2016 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <kernel/sys/rtos.h>
#include <kernel/sys/signal.h>
#include <kernel/sys/utils.h>
#include <kernel/sched/scheduler.h>

L1_CODE void signal(int signum, void (* handler)(int))
{
	int flags;
	local_irq_save(flags);
	{
		kassert(running);
		CyanTCB *tcb = sched.run;

		/* allocate memory for signal node */
		SignalNode *node = malloc_mt(sizeof(SignalNode));

		if(!node)
		{
			printk("cannot register signal handler: out of memory\r\n");
			return;
		}

		/* copy properties */
		node->signum  = signum;
		node->handler = handler;

		/* insert the handler to the given TCB's handler routines */
		spinLock(&tcb->signalHandlersLock); {
			STAILQ_INSERT_TAIL(&tcb->signalHandlers, node, next);
		} spinUnlock(&tcb->signalHandlersLock);
	}
	local_irq_restore(flags);
}

L1_CODE void cyanSendSignal(struct CyanTCB *dest, int signum)
{
	int flags;
	local_irq_save(flags);
	{
		kassert(dest);

		/* allocate memory for the signal node */
		SignalNode *node = malloc_mt(sizeof(SignalNode));

		if(!node)
		{
			printk("cannot send signal: out of memory\r\n");
			return;
		}

		/* we don't need handler since we's just notifying the task */
		node->handler = NULL;
		node->signum = signum;

		/* now we can just simply insert this signal to
		 * the task's signal queue */
		spinLock(&dest->signalsLock); {
			STAILQ_INSERT_TAIL(&dest->signals, node, next);
		} spinUnlock(&dest->signalsLock);
	}
	local_irq_restore(flags);
}

L1_CODE void cyanCheckSignal(void)
{
	int flags;
	local_irq_save(flags);

	// XXX: rewrite this shit

	/* check consistency of the context switch routine and scheduler,
	 * remember that we are now in the middle of the context switch routine */
	//kassert(cyanTcbCurrent == sched.run);

	CyanTCB *tcb = cyanTcbCurrent;//sched.run;

	VECTOR(SignalNode *) sigs;
	VECTOR(SignalNode *) handlers;

	SV_INIT(&sigs, 2, SignalNode *);
	SV_INIT(&handlers, 2, SignalNode *);

	SignalNode *_signal;
	SignalNode *_handler;

	STAILQ_FOREACH(_signal, &tcb->signals, next)
	{
		STAILQ_FOREACH(_handler, &tcb->signalHandlers, next)
		{
			if(_signal->signum == _handler->signum)
			{
				SV_PUSH_BACK(&handlers, _handler);
				SV_PUSH_BACK(&sigs, _signal);
			}
		}
	}

	for(int i = 0; i < SV_SIZE(&sigs); i++)
	{
		STAILQ_REMOVE(&tcb->signals, SV_AT(&sigs, i), SignalNode, next);
		free_mt(SV_AT(&sigs, i));
	}

	/* call handlers */
	for(int i = 0; i < SV_SIZE(&handlers); i++)
	{
		SV_AT(&handlers, i)->handler(SV_AT(&handlers, i)->signum);
	}

	SV_RELEASE(&sigs);
	SV_RELEASE(&handlers);

	local_irq_restore(flags);
}
