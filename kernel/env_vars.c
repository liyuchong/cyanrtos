/*
 * Copyright (c) 2013, 2015-2016 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <string.h>

#include <lib/tree/avl.h>
#include <kernel/mt/memory.h>
#include <kernel/sys/sys.h>

static avl_tree_t __envTree;

static int cmp(const void *a, const void *b)
{
    int ret = strcmp(((const EnvEntry *)a)->key, ((const EnvEntry *)b)->key);

    if(ret > 0)
    {
        return 1;
    }
    else if(ret < 0)
    {
        return -1;
    }
    else
    {
        return 0;
    }
}

int __envinit(void)
{
    avl_create(&__envTree, cmp, sizeof(EnvEntry), offsetof(EnvEntry, link));

    return 0;
}

/**
 *  Search an EnvEntry in the AVL tree
 *
 *  @param name Name of the environment variable
 *
 *  @return EnvEntry of the environment variable, NULL if not found
 */
static EnvEntry *__searchNode(const char *name)
{
    EnvEntry search;

    /* allocate memory for key */
    search.key = malloc_mt(strlen(name) + 1);

    if(!search.key)
    {
        /* out of memory */
        return NULL;
    }

    /* copy key */
    strcpy(search.key, name);

    /* search the AVL tree */
    EnvEntry *result = avl_find(&__envTree, &search, NULL);

    /* free key */
    free_mt(search.key);

    return result;
}

char *__getenv(const char *name)
{
    EnvEntry *result = __searchNode(name);

    /* return value if found, NULL otherwise */
    return result ? result->value : NULL;
}

int __setenv(const char *name, const char *value, int overwrite)
{
    /* check if the variable exists */
    EnvEntry *current = __searchNode(name);
    if(current)
    {
        if(overwrite == 1)
        {
            /* overwrite, remove the original entry and insert a new one */
            avl_remove(&__envTree, current);

            free_mt(current->key);
            free_mt(current->value);
            free_mt(current);
        }
        else
        {
        	/* variable exists, and do not overwrite */

            return 2;
        }
    }

    /* allocate memory for node */
    EnvEntry *node = malloc_mt(sizeof(EnvEntry));

    /* out of memory */
    if(!node)
    {
        goto __setenv_oom;
    }

    /* allocate memory for key and value */
    node->key = malloc_mt(strlen(name) + 1);
    node->value = malloc_mt(strlen(value) + 1);

    /* out of memory */
    if(!node->key || !node->value)
    {
        goto __setenv_oom;
    }

    /* copy key and value */
    strcpy(node->key, name);
    strcpy(node->value, value);

    /* add to tree */
    avl_add(&__envTree, node);

    /* success */
    return 0;

    /* out of memory */
__setenv_oom:

    if(node)
    {
        /* free key */
        if(node->key)
        {
            free_mt(node->key);
            node->key = NULL;
        }

        /* free value */
        if(node->value)
        {
            free_mt(node->value);
            node->value = NULL;
        }

        /* node itself is the last one to be freed */
        free_mt(node);
        node = NULL;
    }

    return 1;
}
