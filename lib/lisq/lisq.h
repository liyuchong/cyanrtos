/*
 * Copyright (c) 2013, 2015-2016 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef LISQ_INTERPRETER_H__
#define LISQ_INTERPRETER_H__

#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <lib/tlsf/tlsf.h>
#include <kernel/arch/blackfin/intdef.h>

enum
{
    QAQ_OK,
    QAQ_ERR,
    QAQ_RETURN,
    QAQ_BREAK,
    QAQ_CONTINUE
};

enum
{
    PT_ESC,
    PT_STR,
    PT_CMD,
    PT_VAR,
    PT_SEP,
    PT_EOL,
    PT_EOF
};

struct LisqParser
{
    char *text;
    char *p; /* current text position */
    int len; /* remaining length */
    char *start; /* token start */
    char *end; /* token end */
    int type; /* token type, PT_... */
    int insidequote; /* True if inside " " */
};

struct LisqVar
{
    char *name, *val;
    struct LisqVar *next;
};

struct LisqInterpreter; /* forward declaration */

typedef int (*LisqCmdFunc)(struct LisqInterpreter *i,
    int argc, char **argv, void *privdata
);

struct LisqCommand
{
    char *name;
    LisqCmdFunc func;
    char **privdata;
    struct LisqCommand *next;
};

struct LisqCallFrame
{
    struct LisqVar *vars;
    
    /* parent is NULL at top level */
    struct LisqCallFrame *parent;
};

struct LisqInterpreter
{
    /* Level of nesting */
    int level;
    struct LisqCallFrame *callframe;
    struct LisqCommand *commands;
    char *result;
    
    /* memory stuff, by using independent memory pools and counters,
     * the very own memory allocator isolates the memory operations from
     * the operating system, this prevents memory leaks and fragmentation. */
    
    /* memory operation count, should be 0 upon exit */
    int       mem_opc;
    /* TLSF pool pointer */
    tlsf_pool mem_pool;
    /* size of the pool */
    size_t    mem_size;
    
    /* the actual pool allocated by the system */
    uint8_t  *qaq_pool;
};

int lisqRegisterCommand(struct LisqInterpreter *i, char *name, LisqCmdFunc f, void *privdata);

int lisqEval(struct LisqInterpreter *i, char *t);

void lisqIntepreterInit(struct LisqInterpreter *i);

void lisqSetResult(struct LisqInterpreter *i, char *s);

struct LisqVar *lisqGetVar(struct LisqInterpreter *i, char *name);
int lisqSetVar(struct LisqInterpreter *i, char *name, char *val);






int lisqArityErr(struct LisqInterpreter *i, char *name);

void lisqSetResult(struct LisqInterpreter *i, char *s);




size_t lisq_mem_alloc(struct LisqInterpreter *i, size_t size);

int lisq_mem_destroy(struct LisqInterpreter *i);

void free_lisq(struct LisqInterpreter *i, void *ptr);

void *malloc_lisq(struct LisqInterpreter *i, size_t size);

void *realloc_lisq(struct LisqInterpreter *i, void *ptr, size_t size);

char *strdup_lisq(struct LisqInterpreter *i, const char *s);

#endif /* LISQ_INTERPRETER_H__ */
