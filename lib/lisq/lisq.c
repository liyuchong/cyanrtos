/* Tcl in ~ 500 lines of code by Salvatore antirez Sanfilippo. BSD licensed */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "lisq.h"
#include <kernel/sys/kern_lib.h>

void lisqInitParser(struct LisqParser *p, char *text)
{
    p->text = p->p = text;
    p->len = (int)strlen(text);
    p->start = 0; p->end = 0; p->insidequote = 0;
    p->type = PT_EOL;
}

int lisqParseSep(struct LisqParser *p)
{
    p->start = p->p;
    
    while(*p->p == ' ' || *p->p == '\t' || *p->p == '\n' || *p->p == '\r')
    {
        p->p++;
        p->len--;
    }
    
    p->end = p->p-1;
    p->type = PT_SEP;
    
    return QAQ_OK;
}

int lisqParseEol(struct LisqParser *p)
{
    p->start = p->p;
    
    while(*p->p == ' ' || *p->p == '\t' || *p->p == '\n' || *p->p == '\r' ||
          *p->p == ';')
    {
        p->p++;
        p->len--;
    }

    p->end = p->p-1;
    p->type = PT_EOL;
    
    return QAQ_OK;
}

int lisqParseCommand(struct LisqParser *p)
{
    int level = 1;
    int blevel = 0;
    p->start = ++p->p; p->len--;
    while (1)
    {
        if (p->len == 0)
        {
            break;
        }
        else if (*p->p == '(' && blevel == 0)
        {
            level++;
        }
        else if (*p->p == ')' && blevel == 0)
        {
            if (!--level) break;
        }
        else if (*p->p == '\\')
        {
            p->p++; p->len--;
        }
        else if (*p->p == '{')
        {
            blevel++;
        }
        else if (*p->p == '}')
        {
            if (blevel != 0) blevel--;
        }
        
        p->p++;
        p->len--;
    }
    
    p->end = p->p-1;
    p->type = PT_CMD;
    
    if (*p->p == ')')
    {
        p->p++;
        p->len--;
    }
    
    return QAQ_OK;
}

int lisqParseVar(struct LisqParser *p)
{
    p->start = ++p->p;
    p->len--; /* skip the $ */
    
    while(1)
    {
        if ((*p->p >= 'a' && *p->p <= 'z') || (*p->p >= 'A' && *p->p <= 'Z') ||
            (*p->p >= '0' && *p->p <= '9') || *p->p == '_')
        {
            p->p++; p->len--; continue;
        }
        break;
    }
    if (p->start == p->p)
    { /* It's just a single char string "$" */
        p->start = p->end = p->p-1;
        p->type = PT_STR;
    }
    else
    {
        p->end = p->p-1;
        p->type = PT_VAR;
    }
    return QAQ_OK;
}

int lisqParseBrace(struct LisqParser *p)
{
    int level = 1;
    p->start = ++p->p; p->len--;
    while(1)
    {
        if (p->len >= 2 && *p->p == '\\')
        {
            p->p++; p->len--;
        }
        else if (p->len == 0 || *p->p == '}')
        {
            level--;
            if (level == 0 || p->len == 0)
            {
                p->end = p->p-1;
                
                if (p->len)
                {
                    p->p++; p->len--; /* Skip final closed brace */
                }
                
                p->type = PT_STR;
                
                return QAQ_OK;
            }
        }
        else if (*p->p == '{')
        {
            level++;
        }
        
        p->p++;
        p->len--;
    }
}

int lisqParseString(struct LisqParser *p)
{
    int newword = (p->type == PT_SEP || p->type == PT_EOL || p->type == PT_STR);
    
    if (newword && *p->p == '{')
        return lisqParseBrace(p);
    else if (newword && *p->p == '"')
    {
        p->insidequote = 1;
        p->p++; p->len--;
    }
    
    p->start = p->p;
    
    while(1)
    {
        if (p->len == 0)
        {
            p->end = p->p-1;
            p->type = PT_ESC;
            
            return QAQ_OK;
        }
        
        switch(*p->p)
        {
            case '\\':
            {
                if (p->len >= 2)
                {
                    p->p++;
                    p->len--;
                }
                
                break;
            }
                
            case '$':
            case '(':
            {
                p->end = p->p-1;
                p->type = PT_ESC;
                
                return QAQ_OK;
            }
                
            case ' ':
            case '\t':
            case '\n':
            case '\r':
            case ';':
            {
                if (!p->insidequote)
                {
                    p->end = p->p-1;
                    p->type = PT_ESC;
                    
                    return QAQ_OK;
                }
                break;
            }
            case '"':
            {
                if (p->insidequote) {
                    p->end = p->p-1;
                    p->type = PT_ESC;
                    p->p++; p->len--;
                    p->insidequote = 0;
                    
                    return QAQ_OK;
                }
                break;
            }
        }
        p->p++; p->len--;
    }
}

int lisqParseComment(struct LisqParser *p)
{
    while(p->len && *p->p != '\n')
    {
        p->p++;
        p->len--;
    }
    
    return QAQ_OK;
}

int lisqGetToken(struct LisqParser *p)
{
    while(1)
    {
        if (!p->len)
        {
            if (p->type != PT_EOL && p->type != PT_EOF)
                p->type = PT_EOL;
            else
                p->type = PT_EOF;
            return QAQ_OK;
        }
        switch(*p->p)
        {
            case ' ':
            case '\t':
            case '\r':
            {
                if (p->insidequote)
                    return lisqParseString(p);
                
                return lisqParseSep(p);
            }
            case '\n':
            case ';':
            {
                if (p->insidequote)
                    return lisqParseString(p);
                
                return lisqParseEol(p);
            }
            case '(':
            {
                return lisqParseCommand(p);
            }
            case '$':
            {
                return lisqParseVar(p);
            }
            case '#':
            {
                if (p->type == PT_EOL)
                {
                    lisqParseComment(p);
                    continue;
                }
                return lisqParseString(p);
            }
            default: return lisqParseString(p);
        }
    }
}

void lisqSetResult(struct LisqInterpreter *i, char *s)
{
    free_lisq(i, i->result);
    
    i->result = strdup_lisq(i, s);
}

struct LisqVar *lisqGetVar(struct LisqInterpreter *i, char *name)
{
    struct LisqVar *v = i->callframe->vars;
    while(v)
    {
        if (strcmp(v->name,name) == 0) return v;
        v = v->next;
    }
    return NULL;
}

int lisqSetVar(struct LisqInterpreter *i, char *name, char *val)
{
    struct LisqVar *v = lisqGetVar(i,name);
    
    if (v)
    {
        free_lisq(i, v->val);
        v->val = strdup_lisq(i, val);
    }
    else
    {
        v = malloc_lisq(i, sizeof(*v));
        v->name = strdup_lisq(i, name);
        v->val = strdup_lisq(i, val);
        v->next = i->callframe->vars;
        i->callframe->vars = v;
    }
    return QAQ_OK;
}

void lisqDropCallFrame(struct LisqInterpreter *i)
{
    struct LisqCallFrame *cf = i->callframe;
    struct LisqVar *v = cf->vars, *t;
    
    while(v)
    {
        t = v->next;
        free_lisq(i, v->name);
        free_lisq(i, v->val);
        free_lisq(i, v);
        v = t;
    }
    
    i->callframe = cf->parent;
    free_lisq(i, cf);
}

int lisqArityErr(struct LisqInterpreter *i, char *name)
{
    char buf[256];
    snprintf(buf, 256,"wrong # args: %s", name);
    lisqSetResult(i,buf);
    return QAQ_ERR;
}

struct LisqCommand *lisqGetCommand(struct LisqInterpreter *i, char *name)
{
    struct LisqCommand *c = i->commands;
    
    while(c)
    {
        if (strcmp(c->name, name) == 0)
            return c;
        
        c = c->next;
    }
    
    return NULL;
}

int lisqRegisterCommand(struct LisqInterpreter *i, char *name, LisqCmdFunc f, void *privdata)
{
    struct LisqCommand *c = lisqGetCommand(i, name);
    char errbuf[256];
    
    if (c)
    {
        snprintf(errbuf, 256, "Command '%s' already defined", name);
        lisqSetResult(i,errbuf);
        return QAQ_ERR;
    }
    
    c = malloc_lisq(i, sizeof(*c));
    c->name = strdup_lisq(i, name);
    c->func = f;
    c->privdata = privdata;
    c->next = i->commands;
    i->commands = c;
    return QAQ_OK;
}

#include <kernel/arch/blackfin/cpu.h>
#include <kernel/sys/utils.h>

/* EVAL! */
int lisqEval(struct LisqInterpreter *i, char *t)
{
    struct LisqParser p;
    int argc = 0, j;
    char **argv = NULL;
    char errbuf[256];
    int retcode = QAQ_OK;
    lisqSetResult(i,"");
    lisqInitParser(&p,t);
    while(1)
    {
        char *t;
        int tlen;
        int prevtype = p.type;
        lisqGetToken(&p);
        if (p.type == PT_EOF) break;
        tlen = (int)(p.end-p.start+1);
        if (tlen < 0) tlen = 0;
        t = malloc_lisq(i, tlen+1);
        memcpy(t, p.start, tlen);
        t[tlen] = '\0';
        if (p.type == PT_VAR) {
            struct LisqVar *v = lisqGetVar(i,t);
            if (!v) {
                snprintf(errbuf,256,"can't read '%s': no such variable",t);
                free_lisq(i, t);
                lisqSetResult(i,errbuf);
                retcode = QAQ_ERR;
                goto err;
            }
            free_lisq(i, t);
            t = strdup_lisq(i, v->val);
        } else if (p.type == PT_CMD) {
            retcode = lisqEval(i,t);
            free_lisq(i, t);
            if (retcode != QAQ_OK) goto err;
            t = strdup_lisq(i, i->result);
        } else if (p.type == PT_ESC) {
            /* XXX: escape handling missing! */
        } else if (p.type == PT_SEP) {
            prevtype = p.type;
            free_lisq(i, t);
            continue;
        }
        /* We have a complete command + args. Call it! */
        if (p.type == PT_EOL)
        {
            struct LisqCommand *c;
            free_lisq(i, t);
            
            prevtype = p.type;
            if (argc)
            {
                if ((c = lisqGetCommand(i,argv[0])) == NULL)
                {
                	// XXX: command not found

                	kassert2(CONTEXT_SWITCH_IRQ_ENABLED);
                	/* execute external command */
                	int ret = exec_args(argc, (const char **)argv);

                	if(ret)
                	{
                        snprintf(errbuf,256,"invalid command name '%s'",argv[0]);
                        lisqSetResult(i,errbuf);
                        retcode = QAQ_ERR;

                	}

                	goto err;

                }
                retcode = c->func(i,argc,argv,c->privdata);
                if (retcode != QAQ_OK) goto err;
            }
            /* Prepare for the next command */
            for (j = 0; j < argc; j++)
            {
                free_lisq(i, argv[j]);
            }
            
            free_lisq(i, argv);
            
            argv = NULL;
            argc = 0;
            
            continue;
        }
        /* We have a new token, append to the previous or as new arg? */
        if (prevtype == PT_SEP || prevtype == PT_EOL)
        {
            argv = realloc_lisq(i, argv, sizeof(char*)*(argc+1));
            argv[argc] = t;
            argc++;
        }
        else
        { /* Interpolation */
            int oldlen = (int)strlen(argv[argc-1]),
            tlen = (int)strlen(t);
            argv[argc-1] = realloc_lisq(i, argv[argc-1], oldlen+tlen+1);
            memcpy(argv[argc-1]+oldlen, t, tlen);
            argv[argc-1][oldlen+tlen]='\0';
            free_lisq(i, t);
        }
        prevtype = p.type;
    }
err:
    for (j = 0; j < argc; j++)
        free_lisq(i, argv[j]);
    free_lisq(i, argv);
    
    return retcode;
}

extern void lisqRegisterCoreCommands(struct LisqInterpreter *i);

void lisqIntepreterInit(struct LisqInterpreter *i)
{
    i->level = 0;
    i->callframe = malloc_lisq(i, sizeof(struct LisqCallFrame));
    i->callframe->vars = NULL;
    i->callframe->parent = NULL;
    i->commands = NULL;
    i->result = strdup_lisq(i, "");
    
    lisqRegisterCoreCommands(i);
}
