/*
 * Copyright (c) 2013, 2015-2016 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

/* Tcl in ~ 500 lines of code by Salvatore antirez Sanfilippo. BSD licensed */

#include "lisq.h"
#include <string.h>
#include <stdio.h>
#include <kernel/sys/kern_lib.h>

extern void lisqSetResult(struct LisqInterpreter *i, char *s);
extern void lisqDropCallFrame(struct LisqInterpreter *i);
extern int lisqArityErr(struct LisqInterpreter *i, char *name);

int lisqCmdReturnCode(struct LisqInterpreter *i, int argc, char **argv, void *pd)
{
    if (argc != 1)
    {
        return lisqArityErr(i, argv[0]);
    }
    
    if (strcmp(argv[0], "break") == 0)
    {
        return QAQ_BREAK;
    }
    else if (strcmp(argv[0], "continue") == 0)
    {
        return QAQ_CONTINUE;
    }
    
    return QAQ_OK;
}

int lisqCmdMath(struct LisqInterpreter *i, int argc, char **argv, void *pd)
{
    char buf[64];
    int a, b, c;
    
    if (argc != 3)
    {
        return lisqArityErr(i,argv[0]);
    }
    
    a = atoi(argv[1]);
    b = atoi(argv[2]);
    
    if (argv[0][0] == '+') c = a+b;
    else if (argv[0][0] == '-')
    {
    	c = a - b;
    }
    else if (argv[0][0] == '*')
    {
    	c = a * b;
   	}
    else if (argv[0][0] == '/')
    {
    	c = a / b;
    }
    else if (argv[0][0] == '>' && argv[0][1] == '\0')
    {
    	c = a > b;
    }
    else if (argv[0][0] == '>' && argv[0][1] == '=')
    {
    	c = a >= b;
    }
    else if (argv[0][0] == '<' && argv[0][1] == '\0')
    {
    	c = a < b;
    }
    else if (argv[0][0] == '<' && argv[0][1] == '=')
    {
    	c = a <= b;
    }
    else if (argv[0][0] == '=' && argv[0][1] == '=')
    {
    	/* if both a && b is number, use ==
    	 * else we use strcmp.
    	 * */
    	if(isnum(argv[1]) && isnum(argv[2]))
    	{
    		c = a == b;
    	}
    	else
    	{
    		c = (strcmp(argv[1], argv[2]) == 0);
    	}
   	}
    else if (argv[0][0] == '!' && argv[0][1] == '=')
    {
    	c = a != b;
    }
    else
    {
    	c = 0;
    }
    
    snprintf(buf, 64, "%d", c);
    
    lisqSetResult(i,buf);
    
    return QAQ_OK;
}

int lisqCmdSet(struct LisqInterpreter *i, int argc, char **argv, void *pd)
{
    if (argc != 3)
        return lisqArityErr(i, argv[0]);
    
    lisqSetVar(i, argv[1], argv[2]);
    lisqSetResult(i, argv[2]);
    
    return QAQ_OK;
}

int lisqCmdEcho(struct LisqInterpreter *i, int argc, char **argv, void *pd)
{
    if (argc != 2)
        return lisqArityErr(i,argv[0]);
    
    // XXX: really new line here? or do we add a `newline' command?
    qprintf("%s\r\n", argv[1]);
    
    return QAQ_OK;
}

int lisqCmdIf(struct LisqInterpreter *i, int argc, char **argv, void *pd)
{
    int retcode;
    if (argc != 3 && argc != 5)
        return lisqArityErr(i,argv[0]);
    
    if ((retcode = lisqEval(i,argv[1])) != QAQ_OK)
        return retcode;
    
    if (atoi(i->result))
        return lisqEval(i,argv[2]);
    else if (argc == 5)
        return lisqEval(i,argv[4]);
    
    return QAQ_OK;
}

int lisqCmdWhile(struct LisqInterpreter *i, int argc, char **argv, void *pd)
{
    if (argc != 3) return lisqArityErr(i,argv[0]);
    
    while(1)
    {
        int ret = lisqEval(i, argv[1]);
        
        if (ret != QAQ_OK)
            return ret;
        
        if (atoi(i->result))
        {
            if ((ret = lisqEval(i, argv[2])) == QAQ_CONTINUE)
                continue;
            else if (ret == QAQ_OK)
                continue;
            else if (ret == QAQ_BREAK)
                return QAQ_OK;
            else
                return ret;
        }
        else
        {
            return QAQ_OK;
        }
    }
}

int lisqCmdCallProc(struct LisqInterpreter *i, int argc, char **argv, void *pd)
{
    char **x = pd, *alist = x[0], *body = x[1];
    char *p = strdup_lisq(i, alist);
    char *tofree;
    
    struct LisqCallFrame *cf = malloc_lisq(i, sizeof(*cf));
    
    int arity = 0, done = 0, errcode = QAQ_OK;
    char errbuf[256];
    
    cf->vars = NULL;
    cf->parent = i->callframe;
    i->callframe = cf;
    tofree = p;
    
    while(1)
    {
        char *start = p;
        
        while(*p != ' ' && *p != '\0')
        {
            p++;
        }
        
        if (*p != '\0' && p == start)
        {
            p++;
            continue;
        }
        
        if (p == start)
            break;
        
        if (*p == '\0')
            done = 1;
        else
            *p = '\0';
        
        if (++arity > argc-1)
            goto arityerr;
        
        lisqSetVar(i,start,argv[arity]);
        
        p++;
        
        if (done)
            break;
    }
    
    free_lisq(i, tofree);
    
    if (arity != argc-1)
        goto arityerr;
    
    errcode = lisqEval(i,body);
    
    if (errcode == QAQ_RETURN)
        errcode = QAQ_OK;
    
    lisqDropCallFrame(i); /* remove the called proc callframe */
    
    return errcode;
    
arityerr:
    snprintf(errbuf,256,"Proc '%s' called with wrong arg num", argv[0]);
    lisqSetResult(i,errbuf);
    lisqDropCallFrame(i); /* remove the called proc callframe */
    return QAQ_ERR;
}

int lisqCmdProc(struct LisqInterpreter *i, int argc, char **argv, void *pd)
{
    char **procdata = malloc_lisq(i, sizeof(char*)*2);

    if (argc != 4)
    	return lisqArityErr(i,argv[0]);

    procdata[0] = strdup_lisq(i, argv[2]); /* arguments list */
    procdata[1] = strdup_lisq(i, argv[3]); /* procedure body */

    return lisqRegisterCommand(i,argv[1],lisqCmdCallProc,procdata);
}

int lisqCmdReturn(struct LisqInterpreter *i, int argc, char **argv, void *pd)
{
    if (argc != 1 && argc != 2)
    	return lisqArityErr(i,argv[0]);

    lisqSetResult(i, (argc == 2) ? argv[1] : "");

    return QAQ_RETURN;
}

void lisqRegisterCoreCommands(struct LisqInterpreter *i)
{
    char *name[] = {
        "+", "-", "*", "/", ">", ">=", "<", "<=", "==", "!="
    };
    
    for (int j = 0; j < (int)(sizeof(name) / sizeof(char* )); j++)
    {
        lisqRegisterCommand(i,name[j],lisqCmdMath,NULL);
    }
    
    lisqRegisterCommand(i,"set",lisqCmdSet,NULL);
    lisqRegisterCommand(i,"=",lisqCmdSet,NULL);
    lisqRegisterCommand(i,"echo",lisqCmdEcho,NULL);
    lisqRegisterCommand(i,"if",lisqCmdIf,NULL);
    lisqRegisterCommand(i,"while",lisqCmdWhile,NULL);
    lisqRegisterCommand(i,"break",lisqCmdReturnCode,NULL);
    lisqRegisterCommand(i,"continue",lisqCmdReturnCode,NULL);
    lisqRegisterCommand(i,"proc",lisqCmdProc,NULL);
    lisqRegisterCommand(i,"return",lisqCmdReturn,NULL);
}
