/*
 * Copyright (c) 2013, 2015-2016 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

/* Tcl in ~ 500 lines of code by Salvatore antirez Sanfilippo. BSD licensed */

#include <stdio.h>
#include "lisq.h"

#include <lib/tlsf/tlsf.h>

#include <kernel/sys/vector.h>

/**
 *  These two functions are used to allocate memory and free memory from the
 *  OS level, implement them based on the the specific OS.
 *
 *  Standard malloc() and free() behaviors defined in ISO/IEC 9899:1999 are
 *  expected.
 */
static inline void *sys_malloc(size_t size)
{
    return malloc_mt(size);
}

static inline void sys_free(void *ptr)
{
    free_mt(ptr);
}

size_t lisq_mem_alloc(struct LisqInterpreter *i, size_t size)
{
    i->mem_opc = 0;
    i->mem_size = size;
    
    /* get pool from system heap */
    i->qaq_pool = sys_malloc(i->mem_size);
    
    if(!i->qaq_pool)
    {
        return 0;
    }
    
    /* create a pool for TLSF allocator */
    i->mem_pool = tlsf_create(i->qaq_pool, size);
    
    /* return size allocated */
    return size;
}

int lisq_mem_destroy(struct LisqInterpreter *i)
{
    sys_free(i->qaq_pool);
    
    return i->mem_opc;
}

void free_lisq(struct LisqInterpreter *i, void *ptr)
{
    if(ptr)
    {
        i->mem_opc--;
    }
    
    tlsf_free(i->mem_pool, ptr);
}

void *malloc_lisq(struct LisqInterpreter *i, size_t size)
{
    i->mem_opc++;
    
    void *p = tlsf_malloc(i->mem_pool, size);
    
    return p;
}

void *realloc_lisq(struct LisqInterpreter *i, void *ptr, size_t size)
{
    if(!ptr)
    {
        i->mem_opc++;
    }
    
    void *p = tlsf_realloc(i->mem_pool, ptr, size);
    
    return p;
}

char *strdup_lisq(struct LisqInterpreter *i, const char *s)
{
    char *res = malloc_lisq(i, strlen(s) + 1);
    strcpy(res, s);
    
    return res;
}
