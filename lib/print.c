/*
 * Copyright (c) 2013, 2015-2016 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "rtos_print.h"

static char buffer[PRINTF_LINE_BUF_LEN];
static PrintCharFunc __uputchar = NULL;

void uprintfInit(PrintCharFunc func)
{
	__uputchar = func;
}

int uputchar(char c)
{
	/* make sure pint char function assigned */
	if(!__uputchar)
	{
		return -1;
	}

	__uputchar(c);

	return 0;
}

int uprintf(char *fmt, ...)
{
	if(!__uputchar)
	{
		return -1;
	}

	int i, count;

    va_list args;
    va_start(args, fmt);
    count = vsnprintf(buffer, PRINTF_LINE_BUF_LEN, fmt, args);
    va_end(args);

    for(i = 0; i < count; i++)
    {
    	__uputchar(buffer[i]);
    }

    return count;
}

int uputs(char *str, int len)
{
    for(int i = 0; i < len; i++)
    {
    	__uputchar(str[i]);
    }

    return 0;
}
