# README #

## ABOUT THE PROJECT ##
This is a realtime operating system intended for control and compute  
intensive applications.  
I started this project in 2013, on a X86 machine. After I implemented some  
basic functions, I stopped maintaining it. Later I tried to port it to  
Blackfin DSPs, since most of my projects use this DSP. In 2015 I started to  
rewrite most of the components. So I will add whatever pops out from my mind  
to it.

## TOOLS ##
I use CCES(Analog Devices embedded development tool based on Eclipse). GCC  
should work, but I will keep using CCES since there's no GCC support for  
the newer Blackfin+ DSPs.

## LICENSE ##

Copyright (c) 2013, 2015 Yuchong Li (93@liyc.me)  
All rights reserved.  

Redistribution and use in source and binary forms, with or without  
modification, are permitted provided that the following conditions  
are met:  
1. Redistributions of source code must retain the above copyright  
notice, this list of conditions and the following disclaimer.  
2. Redistributions in binary form must reproduce the above copyright  
notice, this list of conditions and the following disclaimer in the  
documentation and/or other materials provided with the distribution.  

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND  
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR  
PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS  
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR  
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF  
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS  
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN  
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)  
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF  
THE POSSIBILITY OF SUCH DAMAGE.  

## Don't read me ##
讨厌了啦，别盯着我看了啦